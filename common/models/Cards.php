<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the model class for table "cards".
 *
 * @property int $id
 * @property string $number
 * @property string $bank
 * @property string $mm
 * @property string $yy
 * @property string $cvv
 * @property string $kh_name
 * @property string $state
 * @property string $city
 * @property string $zip
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $email_pass
 * @property string $recovery_email
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 */
class Cards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number',  'mm', 'yy', 'cvv', 'kh_name', 'state', 'city', 'zip', 'address', 'phone', 'email', 'email_pass', 'recovery_email',  'price'  ], 'required'],
            [['created_at', 'updated_at'], 'string'],
            [['number', 'bin','mm', 'yy', 'cvv', 'kh_name', 'state', 'city', 'zip', 'address', 'phone', 'email', 'email_pass', 'recovery_email', 'comment'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['price'], 'double'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'bin' => 'BIN',
            'mm' => 'Mm',
            'yy' => 'Yy',
            'cvv' => 'Cvv',
            'kh_name' => 'Имя владельца',
            'state' => 'Штат',
            'user_id'=>'Добавил',
            'city' => 'Город',
            'zip' => 'Zip',
            'address' => 'Адрес',
            'phone' => 'Phone',
            'email' => 'Email',
            'email_pass' => 'Email Pass',
            'recovery_email' => 'Recovery Email',
            'comment' => 'Коммантарий',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'Status' => 'Статус',
            'price' => 'Цена',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            if(empty($this->created_at))
            {
                $this->created_at = new Expression('NOW()');
            }
            $this->user_id = Yii::$app->user->id;
        } else {
            $this->updated_at = new Expression('NOW()');
        }
        $this->bin = str_split($this->number,6)[0];


        return parent::beforeSave($insert);
    }

    public static function getAllSum()
    {
        $cards = Cards::findAll(['user_id'=>YII::$app->user->id]);
        $sum = 0;
        foreach ($cards as $card)
        {
            $sum += $card->price;
        }
        return $sum;
    }

}
