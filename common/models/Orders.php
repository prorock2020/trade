<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $number
 * @property string $product
 * @property string|null $tracking
 * @property string $price
 * @property string $wasted
 * @property int $percent
 * @property int $card_id
 * @property string|null $drop_name
 * @property string|null $drop_address
 * @property string|null $comment
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $status
 */
class Orders extends \yii\db\ActiveRecord
{

     const STATUS_CONFIRMED = 0;
    const STATUS_SHIP = 1;
    const STATUS_COMPLETE = 2;
    const STATUS_CANCEL = 3;
    const STATUS_INVALID_DELIV = 4;

    public function getKh()
    {
        return $this->hasOne(Cards::className(), ['id' => 'card_id']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_CONFIRMED => "Готовится к отправке",
            self::STATUS_SHIP => "Отправлен",
            self::STATUS_COMPLETE => "Доставлен",
            self::STATUS_CANCEL => "Отменен",
            self::STATUS_INVALID_DELIV => "Развернули"
        ];
    }
    public static function getStatusColors()
    {
        return [
            self::STATUS_CONFIRMED => "light",
            self::STATUS_SHIP => "warning",
            self::STATUS_COMPLETE => "success",
            self::STATUS_CANCEL => "danger",
            self::STATUS_INVALID_DELIV => "danger"
        ];
    }
    public static function getColor($status_id)
    {
        $statuses = self::getStatusColors();

        return $statuses[$status_id];
    }
    /**
     * @param $status_id
     */
    public static function getStatus($status_id)
    {
        $statuses = self::getStatusList();

        return $statuses[$status_id];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'product', 'price', 'percent', 'card_id','wasted'], 'required'],
            [['product', 'drop_address', 'created_at', 'updated_at'], 'string'],
            [['percent', 'card_id', 'status'], 'integer'],
            [['number', 'tracking', 'price', 'drop_name', 'comment'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['price'],'double'],
            [['wasted'],'double']
        ];
    }

    public static function getAll()
    {
        return \common\models\Orders::find()->where(["user_id"=>Yii::$app->user->id])->orderBy(["created_at"=>"DESC"])->all();
    }
    public static function getShipment()
    {
        return \common\models\Orders::findAll([
            "user_id"=>Yii::$app->user->id,
            "status"=>self::STATUS_SHIP
        ]);
    }
    public static function getCancel()
    {
        return \common\models\Orders::findAll([
            "user_id"=>Yii::$app->user->id,
            "status"=>[self::STATUS_CANCEL,self::STATUS_INVALID_DELIV]
        ]);
    }
    public static function getConfirmed()
    {
        return \common\models\Orders::findAll([
            "user_id"=>Yii::$app->user->id,
            "status"=>self::STATUS_CONFIRMED
        ]);
    }
    public static function getSuccess()
    {
        return \common\models\Orders::findAll([
            "user_id"=>Yii::$app->user->id,
            "status"=>self::STATUS_COMPLETE
        ]);
    }

    /**
     * @return bool
     */
    public function isCancel()
    {
        if($this->status == self::STATUS_CANCEL  || $this->status == self::STATUS_INVALID_DELIV)
        {
            return true;
        }
        return false;
    }
    /**
     * @param self $orders
     */
    public static function getSum($orders)
    {
        $sum = 0;
        foreach ($orders as $order)
        {
            $sum += $order->calcSum();
        }
        return $sum;
    }


    /**
     * @return float|int
     */
    public function calcSum()
    {
        return $this->price/100*$this->percent;
    }
    /**
     * @param self $orders
     */
    public static function getWasted($orders)
    {
        $sum = 0;
        foreach ($orders as $order)
        {
            $sum += $order->wasted;
        }
        return $sum;
    }
    /**
     * @param self $orders
     */
    public static function getProgressOrders()
    {

        $orders = self::find()->where(
            ["<>","status",self::STATUS_CANCEL]
        )
        ->andWhere(
            ["<>","status",self::STATUS_INVALID_DELIV]

        )->all();

        return $orders;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер заказа',
            'product' => 'Название товара',
            'tracking' => 'Трек номер',
            'price' => 'Цена',
            'percent' => 'Процент',
            'card_id' => 'Кардхолдер',
            'drop_name' => 'Имя дропа',
            'drop_address' => 'Адрес дропа',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'wasted'=> 'Потрачено'
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {

            if(empty($this->created_at))
            {
                $this->created_at = new Expression('NOW()');
            }
            if(empty($this->status))
            {
                $this->status = self::STATUS_CONFIRMED;
            }

            $this->user_id = Yii::$app->user->id;
        } else {
            $this->updated_at = new Expression('NOW()');
        }


        return parent::beforeSave($insert);
    }
}
