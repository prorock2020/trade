<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "charges".
 *
 * @property int $id
 * @property string|null $info
 * @property string $price
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $user_id
 */
class Charges extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charges';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info','price','created_at'], 'required'],
            [['created_at', 'updated_at'], 'string'],
            [['user_id'], 'integer'],
            [['price'], 'double'],
            [['info'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'info' => 'На что',
            'price' => 'Стоимость',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
    public function beforeSave($insert) {

        if ($this->isNewRecord) {

            if(empty($this->created_at))
            {
                $this->created_at = new Expression('NOW()');
            }

            $this->user_id = Yii::$app->user->id;
        } else {
            $this->updated_at = new Expression('NOW()');
        }


        return parent::beforeSave($insert);
    }

    public static function getAllSum()
    {
        $cards = Charges::findAll(['user_id'=>YII::$app->user->id]);
        $sum = 0;
        foreach ($cards as $card)
        {
            $sum += $card->price;
        }
        return $sum;
    }

}
