<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'view' => [

        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'telegram' => [
            'class' => 'aki\telegram\Telegram',
            'botToken' => '1784761423:AAGTCIp795_F5HUbpeKbGnd6ry4wQ3cAUvA',
        ],
        'payeerApi' => [
            'class' => \yiidreamteam\payeer\Api::class,
            'accountNumber' => '<account number>',
            'apiId' => '<api ID>',
            'apiSecret' => '<your secret>'
        ],
    ],
];
