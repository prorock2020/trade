<?php

use yii\db\Migration;

/**
 * Class m220127_044233_orders
 */
class m220127_044233_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->notNull()->unique(),
            'product'=> $this->text()->notNull(),
            'tracking' => $this->string()->null(),
            'price' => $this->string()->notNull(),
            'percent' => $this->integer()->notNull(),
            'card_id'=>$this->integer()->notNull(),
            'drop_name'=>$this->string()->null(),
            'drop_address'=>$this->text()->null(),
            'comment' => $this->string()->null(),
            'created_at' => $this->text()->null(),
            'updated_at' => $this->text()->null(),
            'status'=>$this->integer()->defaultValue(0),
            'user_id'=> $this->integer()->notNull(),
            'wasted'=>$this->string()->notNull()

        ], null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220127_044233_orders cannot be reverted.\n";

        return false;
    }
    */
}
