<?php

use yii\db\Migration;

/**
 * Class m220127_230451_cards_fields
 */
class m220127_230451_cards_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cards', 'status', $this->integer()->defaultValue(0));
        $this->addColumn('cards', 'price', $this->string()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cards', 'status');
        $this->dropColumn('cards', 'price');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220127_230451_cards_fields cannot be reverted.\n";

        return false;
    }
    */
}
