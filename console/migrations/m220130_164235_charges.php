<?php

use yii\db\Migration;

/**
 * Class m220130_164235_charges
 */
class m220130_164235_charges extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('charges', [
            'id' => $this->primaryKey(),

            'info' => $this->string()->null(),
            'price' => $this->string()->notNull(),
            'created_at' => $this->text()->null(),
            'updated_at' => $this->text()->null(),
            'user_id'=> $this->integer()->notNull()

        ], null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('charges');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220130_164235_charges cannot be reverted.\n";

        return false;
    }
    */
}
