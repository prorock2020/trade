<?php

use yii\db\Migration;

/**
 * Class m220127_004148_card
 */
class m220127_004148_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cards', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->notNull()->unique(),
            'bin' => $this->string()->null(),
            'mm' => $this->string()->notNull(),
            'yy' => $this->string()->notNull(),
            'cvv' => $this->string()->notNull(),
            'kh_name' => $this->string()->notNull(),
            'state' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'zip' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'email_pass' => $this->string()->notNull(),
            'recovery_email' => $this->string()->notNull(),
            'comment' => $this->string()->null(),
            'created_at' => $this->text()->null(),
            'updated_at' => $this->text()->null(),
            'user_id'=> $this->integer()->notNull()

        ], null);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cards');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220127_004148_card cannot be reverted.\n";

        return false;
    }
    */
}
