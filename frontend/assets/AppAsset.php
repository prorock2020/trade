<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/all-public-fa9285e534b085a44cbaa70a3b855064.css',
        'css/banner.css'
    ];
    public $js = [
        '/js/api.js',

    ];
    public $depends = [
       //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
