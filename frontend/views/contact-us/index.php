<?php

/* @var $this yii\web\View */

$this->title = 'Best cryptocurrency pairs to trade – TradeSanta';
?>
<main class="flex-grow-1">
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <input type="hidden" name="_csrf-frontend" value="bLm0LO6UEwHM-sFh01KGhlWo-JkDqQ71IGWK9tj8QQErjsJooPFnaabCm1K2Zr7OA5216XDMRZR0DuGVqb4COQ==">
    <article class="container-fluid py-3 py-md-5">

        <!--  onclick add/remove "modal-disable" class to the 1st div  -->



        <div class="container">
            <div class="contact-us wysiwyg">
                <h1 class="mb-3">Contact us</h1>
                <p class="mb-3">
                    You can communicate with TradeSanta team using email<br>
                    <a href="#">team@tradesantas.com</a>
                    or our                    <a href="#">official Telegram chat</a>
                    .
                    <br>
                    Also you can send your feedback using the form below.                </p>

                <div class="form-row">
                    <div class="form-group col-12 col-md-8 col-lg-6 mr-auto">
                        <label for="user-name">Name</label>
                        <div>
                            <input class="form-control" type="text" id="user-name" name="user-name">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12 col-md-8 col-lg-6 mr-auto">
                        <label for="user-email">Email</label>
                        <div>
                            <input class="form-control" type="text" id="user-email" name="user-email">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12 col-md-8 col-lg-6 mr-auto">
                        <label for="user-subject">Subject</label>
                        <div>
                            <input class="form-control" type="text" id="user-subject" name="user-subject">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12 col-md-8 col-lg-6 mr-auto">
                        <label for="user-message">Message</label>
                        <div>
                            <input rows="10" class="form-control" type="text" id="user-message" name="user-message">
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-12 col-md-8 col-lg-6 mr-auto">
                        <div class="g-recaptcha" data-sitekey="6Leu_HIUAAAAAE22kYvbezJdRo1v3PuIsul6OTFg"><div style="width: 304px; height: 78px;"><div><iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6Leu_HIUAAAAAE22kYvbezJdRo1v3PuIsul6OTFg&amp;co=aHR0cHM6Ly90cmFkZXNhbnRhLmNvbTo0NDM.&amp;hl=en&amp;v=zLD1nfkNCJC1kEswSRdSyd-p&amp;size=normal&amp;cb=fmajbkhs5v3s" role="presentation" name="a-53twbxw45k8h" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox allow-storage-access-by-user-activation" width="304" height="78" frameborder="0"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div><iframe style="display: none;"></iframe></div>
                    </div>
                </div>



                <a href="#" id="btn_send" class="btn btn-primary d-inline-block">
                    Send                </a>

                <div id="error_box" style="margin-top:20px; color:red; display: none">
                    <p>Errors:</p>
                    <div id="error_list">

                    </div>
                </div>

            </div>
        </div>
    </article>
</main>