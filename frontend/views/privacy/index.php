<?php

/* @var $this yii\web\View */

$this->title = 'Privacy – TradeSanta';
?>
<main class="flex-grow-1">
    <article class="container-fluid py-3 py-md-5">
        <div class="container">
            <div class="wysiwyg">
                <h1 class="mb-5">Privacy</h1>        <h3 class="mt-5 mb-3">Preamble</h3>

                <p>TradeSanta (hereafter also “<strong>We</strong>”) offers online software as a service (SaaS) through the website&nbsp;<a href="https://tradesantas.com" style="text-decoration:none;"><u>https://tradesantas.com</u></a> (hereafter – “<strong>Website</strong>”). Our software (hereafter – “<strong>Software</strong>”) enables you to trade and invest in cryptocurrencies by means of an automatic crypto trader bot – of which (solely) you control and configure the settings.</p>

                <p>To provide you with our Services, we might need (and sometimes obliged by the law) to collect your personal data.</p>

                <p>We strictly follow industry best practices and adhere to the rules set forth in the General Data Protection Regulation, OPPA, CAN-SPAM and COPPA.</p>

                <h3 class="mt-5 mb-3">What types of personal information do we collect?</h3>

                <p>While providing our services we collect:</p>

                <ul>
                    <li>
                        <p>IP address</p>
                    </li>
                    <li>
                        <p>Your username</p>
                    </li>
                    <li>
                        <p>Your e-mail address</p>
                    </li>
                </ul>

                <h3 class="mt-5 mb-3">Where do we store and process data of EU citizens?</h3>

                <p>We store and process your data on the territory of the EU. Please note that we identify your jurisdiction based on your IP address.</p>

                <h3 class="mt-5 mb-3">Who is the data controller?</h3>

                <p>TradeSanta</p>

                <h3 class="mt-5 mb-3">Who is the data processor?</h3>

                <p>Digital Ocean, LLC</p>

                <h3 class="mt-5 mb-3">Do you honor “do not track” signals?</h3>

                <p>We do not collect any information that may identify you without your permission. However, we use Google Analytics on our website. If you want to know more about Google Analytics and its “do not track” policy, please visit https://www.google.com/analytics/terms/us.html.</p>

                <h3 class="mt-5 mb-3">Can I modify my personal data?</h3>

                <p>If you are a registered user of our Services, you can do so in your account settings. You may correct, delete, or modify the personal information you provided to us and associated with your account.</p>

                <h3 class="mt-5 mb-3">Do I have a right to access my data?</h3>

                <p>In the case you are a EU citizen and want to exercise your rights enshrined in art. 15 of GDPR, please write us at&nbsp;team@tradesanta.com</p>

                <h3 class="mt-5 mb-3">Law and Harm</h3>

                <p>Notwithstanding anything to the contrary in this Privacy Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation, legal process, or governmental request; to protect the safety of any person; to address fraud, security or technical issues; or to protect our or our users’ rights or property. However, nothing in this Privacy Policy is intended to limit any legal defenses or objections that you may have to a government’s request for disclosure of your information.</p>

                <h3 class="mt-5 mb-3">Right to be forgotten</h3>

                <p>If you want to close your account and delete all information that would allow anyone to identify you, please write us at&nbsp;<a href="mailto:team@tradesanta.com" style="text-decoration:none;"><u>team@tradesanta.com</u></a>. Please note that such a request will also terminate your TradeSanta account. All money stored on your account will be reimbursed according to our Refund Policy.</p>

                <p>&nbsp;</p>

            </div>
        </div>

    </article>
</main></script>