<?php

/* @var $this yii\web\View */

$this->title = 'FAQ – TradeSanta';
?>
<main class="flex-grow-1">
    <article class="container-fluid py-3 py-md-5">
        <div class="container">
            <div class="wysiwyg">
                <h1 class="mb-5"></h1><style> body {
                        min-height: 100vh;
                        min-width: 320px;
                        color: #000;
                        font-family: 'Nunito Sans', 'Open Sans', sans-serif;
                        font-size: 1.25rem;
                        font-weight: 400;
                        -webkit-font-smoothing: antialiased;
                    }

                    .breadcrumb-ts {
                        padding: 0;
                        background-color: transparent;
                    }

                    .breadcrumb-ts .breadcrumb-item {
                        font-size: .938rem;
                    }

                    .breadcrumb-ts .breadcrumb-item a {
                        color: #000;
                    }

                    .breadcrumb-ts .breadcrumb-item + .breadcrumb-item {
                        padding-left: 4px;
                    }

                    .breadcrumb-ts .breadcrumb-item + .breadcrumb-item::before {
                        content: '';
                        width: 5px;
                        height: 100%;
                        padding-right: 26px;
                        background-image: url("../img/breadcrumb.svg");
                        background-repeat: no-repeat;
                        background-position: center;
                    }

                    .breadcrumb-ts--grey .breadcrumb-item a,
                    .breadcrumb-ts--grey .breadcrumb-item.active {
                        color: #A3A3A3;
                    }

                    .faq .page {
                        padding-top: 44px;
                        padding-bottom: 96px;
                    }

                    .faq .title {
                        font-weight: 700;
                        margin-bottom: 0;
                    }

                    .faq p {
                        margin-bottom: 22px;
                        font-size: 1rem;
                        line-height: 22px;
                    }

                    .faq .grid {
                        display: grid;
                        grid-template-columns: 1fr 1fr 1fr;
                        margin-right: 0;
                        margin-left: 0;
                    }

                    .faq__category {
                        padding-top: 40px;
                    }

                    .faq .category__item-title {
                        margin: 0;
                        padding-bottom: 17px;
                        font-weight: 700;
                        font-size: 1.125rem;
                        line-height: 25px;
                    }

                    .faq .category__item-title a {
                        color: #1274BB;
                    }

                    .faq .category__item-list {
                        max-height: 130px;
                        padding-left: 0;
                        list-style-type: none;
                        overflow: hidden;
                        transition: all .5s ease-in-out;
                    }

                    .faq .category__item-list li {
                        margin-bottom: 3px;
                    }

                    .faq .category__item-list li a {
                        font-size: 1rem;
                        line-height: 25px;
                        color: #000;
                    }

                    .faq .category__item-btn {
                        min-width: 120px;
                        height: 32px;
                        padding: 0;
                        background-color: #E4E4E4;
                        border-radius: 20px;
                        font-weight: 700;
                        font-size: 0.875rem;
                        line-height: 32px;
                        color: #212121;
                        outline: none;
                    }

                    .faq .category__item-btn:hover {
                        background-color: #E9E9E9;
                    }

                    .faq-inner .page {
                        padding-bottom: 96px;
                    }

                    .faq-inner h1 {
                        font-weight: 700;
                        margin-bottom: 0;
                    }

                    .faq-inner__nav ul {
                        list-style-type: none;
                        margin: 0;
                        padding: 0;
                        text-align: center;
                        letter-spacing: 10px;
                    }

                    .faq-inner__nav ul .nav-item {
                        display: inline-block;
                        width: 208px;
                        height: 32px;
                        margin-bottom: 20px;
                        border: 2px solid #D8D8D8;
                        border-radius: 40px;
                    }

                    .faq-inner__nav ul .nav-item:hover,
                    .faq-inner__nav ul .nav-item.selected {
                        background-color: #DEDEDE;
                    }

                    .faq-inner__nav ul .nav-item.selected a {
                        font-weight: 700;
                    }

                    .faq-inner__nav ul .nav-item a {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        height: 100%;
                        font-weight: 400;
                        font-size: .813rem;
                        line-height: 18px;
                        color: #000;
                        text-decoration: none;
                        letter-spacing: normal;
                    }

                    .faq-inner .articles-item {
                        padding-top: 19px;
                        padding-bottom: 1px;
                        border-top: 1px solid #BFBFBF;
                        color: #212121;
                    }

                    .faq-inner .articles-item:last-child {
                        border-bottom: 1px solid #BFBFBF;
                    }

                    .faq-inner .articles-item .title {
                        position: relative;
                        margin-bottom: 0;
                        font-weight: 700;
                        font-size: 1rem;
                        line-height: 21px;
                        cursor: pointer;
                    }

                    .faq-inner .articles-item .title::after {
                        content: '';
                        position: absolute;
                        top: 50%;
                        right: 0;
                        margin-top: -3px;
                        width: 11px;
                        height: 6px;
                        background-image: url("../img/arrow.svg");
                        transform: rotate(-90deg);
                        transition: transform .3s ease-out;
                    }

                    .faq-inner .articles-item .content {
                        max-height: 0;
                        overflow: hidden;
                        transition: max-height .3s ease;
                        margin-top: 18px;
                    }

                    .faq-inner .articles-item .content .par {
                        margin-bottom: 14px;
                        font-size: .875rem;
                        line-height: 22px;
                    }

                    .faq-inner .articles-item .content .link {
                        font-weight: 700;
                        font-size: .938rem;
                        line-height: 33px;
                    }

                    .faq-inner .articles-item--active {
                        padding-bottom: 19px;
                    }

                    .faq-inner .articles-item--active .title::after {
                        transform: rotate(0);
                    }

                    .faq-inner .similar-block {
                        margin-top: 56px;
                    }

                    .faq-inner .similar-block .similar-title {
                        margin-bottom: 0;
                        font-weight: 700;
                        font-size: 1.5rem;
                        line-height: 22px;
                        color: #212121;
                    }

                    .faq-inner .similar-block .similar-links {
                        list-style-type: none;
                        display: grid;
                        grid-template-columns: auto auto auto auto;
                        margin-top: 22px;
                        margin-bottom: 0;
                        padding-left: 0;
                    }

                    .faq-inner .similar-block .similar-links li a {
                        font-weight: 700;
                        font-size: 1.125rem;
                        line-height: 28px;
                        color: #1274BB;
                    }

                    @media (min-width: 768px) {
                        .breadcrumb-ts {
                            margin-top: 25px;
                            margin-bottom: 30px;
                        }

                        .faq .title {
                            font-size: 2.5rem;
                            line-height: 52px;
                        }

                        .faq-inner .breadcrumb-ts {
                            margin-top: 25px;
                            margin-bottom: 18px;
                        }

                        .faq-inner h1 {
                            font-size: 2.5rem;
                            line-height: 52px;
                        }

                        .faq-inner__nav {
                            margin-top: 40px;
                            margin-bottom: 27px;
                        }

                        .faq-inner__nav ul {
                            letter-spacing: 26px;
                        }
                    }

                    @media (min-width: 992px) {
                        .faq .grid {
                            grid-gap: 54px 50px;
                        }
                    }

                    @media (min-width: 1200px) {
                        .breadcrumb-ts {
                            margin-top: 33px;
                            margin-bottom: 28px;
                        }

                        .faq .grid {
                            grid-column-gap: 56px;
                        }

                        .faq-inner__nav ul {
                            letter-spacing: 10px;
                        }
                    }

                    @media (max-width: 991px) {
                        .faq-inner .similar-block .similar-links {
                            grid-template-columns: auto auto;
                            grid-row-gap: 13px;
                        }
                    }

                    @media (max-width: 767px) {
                        body {
                            font-size: 0.875rem;
                        }

                        .breadcrumb-ts {
                            margin-top: 20px;
                            margin-bottom: 25px;
                        }

                        .breadcrumb-ts .breadcrumb-item {
                            font-size: .813rem;
                        }

                        .faq .page {
                            padding-top: 30px;
                            padding-bottom: 74px;
                        }

                        .faq .title {
                            font-size: 1.375rem;
                            line-height: 29px;
                        }

                        .faq p {
                            font-size: 0.813rem;
                            line-height: 20px;
                        }

                        .faq .grid {
                            display: flex;
                            flex-direction: column;
                        }

                        .faq__category {
                            padding-top: 34px;
                        }

                        .faq .category__item {
                            margin-bottom: 48px;
                        }

                        .faq .category__item-title {
                            padding-bottom: 14px;
                            font-size: 1rem;
                            line-height: 25px;
                        }

                        .faq .category__item-list li {
                            margin-bottom: 8px;
                        }

                        .faq-inner .page {
                            padding-bottom: 74px;
                        }

                        .faq-inner .breadcrumb-ts {
                            margin-bottom: 16px;
                        }

                        .faq-inner h1 {
                            font-size: 1.375rem;
                            line-height: 29px;
                        }

                        .faq-inner__nav {
                            margin-top: 12px;
                            margin-bottom: 22px;
                        }

                        .faq-inner__nav ul {
                            display: flex;
                            flex-wrap: wrap;
                            justify-content: space-between;
                        }

                        .faq-inner__nav ul .nav-item {
                            width: calc(50% - 4px);
                            min-width: 140px;
                            height: 28px;
                            margin-bottom: 10px;
                            padding-right: 5px;
                            padding-left: 5px;
                        }

                        .faq-inner__nav ul .nav-item:last-child {
                            margin: auto;
                        }

                        .faq-inner__nav ul .nav-item a {
                            font-size: .625rem;
                            line-height: 10px;
                        }

                        .faq-inner .articles-item {
                            padding-top: 14px;
                            padding-bottom: 14px;
                        }

                        .faq-inner .articles-item .title {
                            font-size: .875rem;
                            line-height: 22px;
                        }

                        .faq-inner .articles-item .title::after {
                            right: 5px;
                        }

                        .faq-inner .articles-item .content {
                            margin-top: 14px;
                        }

                        .faq-inner .articles-item .content .par {
                            max-width: 95%;
                            font-size: .813rem;
                            line-height: 18px;
                        }

                        .faq-inner .articles-item .content .link {
                            font-size: .875rem;
                            line-height: 29px;
                        }

                        .faq-inner .similar-block {
                            margin-top: 28px;
                        }

                        .faq-inner .similar-block .similar-title {
                            font-size: 1.125rem;
                            line-height: 16px;
                        }

                        .faq-inner .similar-block .similar-links {
                            margin-top: 16px;
                            grid-template-columns: auto;
                        }

                        .faq-inner .similar-block .similar-links li a {
                            font-size: .875rem;
                            line-height: 22px;
                        }
                    }

                    @media (min-width: 768px) and (max-width: 991px) {
                        .faq .grid {
                            grid-template-columns: 1fr 1fr;
                            grid-gap: 60px 29px;
                        }

                        .faq .category__item-btn {
                            margin-top: 9px;
                        }
                    }
                    /*# sourceMappingURL=maps/main.css.map */
                </style>
                <main class="flex-grow-1 faq">
                    <section class="page">
                        <div class="container">
                            <!-- title page -->
                            <h1 class="title">FAQ</h1>
                            <!-- category's navigation -->
                            <div class="grid faq__category">

                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/create-an-account">Creating an account</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="https://tradesanta.com/en/how-register-tradesanta" target="_blank" rel="noopener">How to register on TradeSanta?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-recover-password" target="_blank" rel="noopener">How to recover password?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-many-plans-do-you-have-and-how-do-they-differ" target="_blank" rel="noopener">How many plans do you have and how do they differ?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-purchase-paid-plan" target="_blank" rel="noopener">How to purchase paid plan?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-long-does-it-take-process-payment" target="_blank" rel="noopener">How long does it take to process the payment?</a></li>
                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/start-trading">Start trading</a></h4>
                                    <ul class="category__item-list toggle active" style="max-height: 365px;">
                                        <li><a href="https://tradesanta.com/en/where-start-cryptocurrency-trading" target="_blank" rel="noopener">Where to start cryptocurrency trading?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-connect-cryptocurrency-exchange" target="_blank" rel="noopener">How to connect a cryptocurrency exchange?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-create-new-bot" target="_blank" rel="noopener">How to create a new bot?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-create-api-key-and-secret-key-hitbtc" target="_blank" rel="noopener">How to create API key and secret key on HitBTC?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-deposit-hitbtc" target="_blank" rel="noopener">How to deposit to HitBTC?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-operate-orders-hitbtc" target="_blank" rel="noopener">How to operate with orders on HitBTC?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-create-api-key-and-secret-key-binance" target="_blank" rel="noopener">How to create API key and secret key on Binance?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-deposit-binance" target="_blank" rel="noopener">How to deposit to Binance?</a></li>
                                        <li><a href="https://tradesanta.com/en/telegram-notifications" target="_blank" rel="noopener">Telegram notifications</a></li>
                                    </ul>
                                   </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/bots-grid-dca-smart-order">Bots: Grid, DCA, Smart Order </a></h4>
                                    <ul class="category__item-list">
                                        <li><a href="https://tradesanta.com/en/grid-vs-dca" target="_blank" rel="noopener">What is the difference between Grid and DCA strategy?</a></li>
                                        <li><a href="https://tradesanta.com/en/grid-bot-tutorial" target="_blank" rel="noopener">Grid Bot: Strategy And Parameters Explained</a></li>
                                        <li><a href="https://tradesanta.com/en/dca-bot" target="_blank" rel="noopener">DCA Bot: Strategy And Parameters Explained</a></li>
                                        <li><a href="https://tradesanta.com/en/smart-order" target="_blank" rel="noopener">What is Smart Order For?</a></li>
                                    </ul>
                                </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/setting-up-bots">Setting up bots</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="/en/how-to-set-up-futures-bot" target="_blank" rel="noopener">How to set up Futures bot</a></li>
                                        <li><a href="https://tradesanta.com/en/trading-view-indicators" target="_blank" rel="noopener">TradingView signals</a></li>
                                        <li><a href="https://tradesanta.com/en/custom-trading-view-indicators" target="_blank" rel="noopener">TradingView custom signals</a></li>
                                        <li><a href="https://tradesanta.com/en/glossary" target="_blank" rel="noopener">TradeSanta Glossary</a></li>
                                        <li><a href="https://tradesanta.com/en/long-vs-short" target="_blank" rel="noopener">What is the difference between long and short strategy</a></li>
                                        <li><a href="https://tradesanta.com/en/errors" target="_blank" rel="noopener">Crypto bot’s errors</a></li>
                                        <li><a href="https://tradesanta.com/en/crypto-bots-parameters" target="_blank" rel="noopener">Crypto bot’s parameters</a></li>
                                        <li><a href="https://tradesanta.com/en/after-i-turn-bot-does-it-start-trading-immediately" target="_blank" rel="noopener">After I turn on the bot does it start trading immediately?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-long-strategy" target="_blank" rel="noopener">What is a long strategy?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-short-strategy" target="_blank" rel="noopener">What is a short strategy?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-set-take-profit" target="_blank" rel="noopener">How to set take profit?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-extra-order" target="_blank" rel="noopener">What is extra order?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-take-profit" target="_blank" rel="noopener">What is take profit?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-bollinger-and-how-it-works-tradesanta-bot" target="_blank" rel="noopener">What is Bollinger and how it works with TradeSanta bot?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-martingale" target="_blank" rel="noopener">What is martingale?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-settings-are-best" target="_blank" rel="noopener">What settings are the best?</a></li>
                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/trading-analytics-on-tradesanta">Trading analytics on TradeSanta</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="https://tradesanta.com/en/how-to-use-dashboard" target="_blank" rel="noopener">How to use Dashboard?</a></li>
                                        <li><a href="https://tradesanta.com/en/i-see-profit-my-bitcoin-btc-balance-goes-down-how-come" target="_blank" rel="noopener">I see profit but my Bitcoin (BTC) balance goes down - how come?</a></li>
                                        <li><a href="https://tradesanta.com/en/does-bot-include-exchanges-fee-profit-calculation" target="_blank" rel="noopener">Does the bot include exchange's fee in profit calculation?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-are-exchange-rates" target="_blank" rel="noopener">What are exchange rates?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-are-candlestick-charts" target="_blank" rel="noopener">What are candlestick charts?</a></li>
                                        <li><a href="https://tradesanta.com/en/errors" target="_blank" rel="noopener">Most common crypto bot errors (Grid &amp; DCA)</a></li>
                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/trading-basics">Trading Basics</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="https://tradesanta.com/en/how-trade" target="_blank" rel="noopener">How to trade?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-much-money-do-i-need-start-trading" target="_blank" rel="noopener">How much money do I need to start trading?</a></li>
                                        <li><a href="https://tradesanta.com/en/will-i-make-profit-are-profits-guaranteed" target="_blank" rel="noopener">Will I make a profit? Are profits guaranteed?</a></li>
                                        <li><a href="https://tradesanta.com/en/do-you-take-commission-orders" target="_blank" rel="noopener">Do you take commission on orders?</a></li>
                                        <li><a href="https://tradesanta.com/en/there-minimum-start-bots-trading" target="_blank" rel="noopener">Is there a minimum to start bots trading?</a></li>
                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/security">Security</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="https://tradesanta.com/en/security-tips" target="_blank" rel="noopener">Security tips</a></li>
                                        <li><a href="https://tradesanta.com/en/are-my-funds-secure" target="_blank" rel="noopener">Are my funds secure?</a></li>
                                        <li><a href="https://tradesanta.com/en/how-protect-your-funds" target="_blank" rel="noopener">How to protect your funds?</a></li>
                                        <li><a href="https://tradesanta.com/en/phishing-attacks" target="_blank" rel="noopener">Phishing attacks</a></li>
                                        <li><a href="https://tradesanta.com/en/anti-virus-and-trojan-guideline" target="_blank" rel="noopener">Anti-virus and trojan guideline</a></li>

                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/about-tradesanta">About TradeSanta</a></h4>
                                    <ul class="category__item-list toggle">
                                        <li><a href="https://tradesanta.com/en/about-us" target="_blank" rel="noopener">What is TradeSanta?</a></li>
                                        <li><a href="https://tradesanta.com/en/tradesanta-cryptocurrency-exchange" target="_blank" rel="noopener">Is TradeSanta a cryptocurrency exchange?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-automated-crypto-trading" target="_blank" rel="noopener"> What is automated crypto trading?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-exchanges-does-service-support" target="_blank" rel="noopener">What exchanges does the service support?</a></li>
                                        <li><a href="https://tradesanta.com/en/what-tradesanta-referral-program" target="_blank" rel="noopener">What is TradeSanta referral program?</a></li>
                                        <li><a href="https://tradesanta.com/en/crypto-trading-app" target="_blank" rel="noopener">Do you have a mobile app?</a></li>
                                    </ul>
                                    </div>
                                <div class="category__item">
                                    <h4 class="category__item-title"><a href="https://tradesanta.com/en/tradesanta-news-and-updates">TradeSanta news and Updates</a></h4>
                                    <ul class="category__item-list">
                                        <li><a href="https://tradesanta.com/en/how-subscribe-and-receive-latest-tradesanta-news" target="_blank" rel="noopener">How to subscribe and receive the latest Tradesanta news?</a></li>
                                        <li><a href="https://tradesanta.com/en/do-you-have-any-social-media-accounts">Do you have social media accounts?</a></li>
                                        <li><a href="https://tradesanta.com/en/do-you-have-any-blog">Do you have a blog?</a></li>
                                        <li><a href="https://tradesanta.com/en/roadmap">TradeSanta's Roadmap</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </section>
                </main>



                <script>
                    /******/ (function(modules) { // webpackBootstrap
                        /******/ 	// The module cache
                        /******/ 	var installedModules = {};
                        /******/
                        /******/ 	// The require function
                        /******/ 	function __webpack_require__(moduleId) {
                            /******/
                            /******/ 		// Check if module is in cache
                            /******/ 		if(installedModules[moduleId]) {
                                /******/ 			return installedModules[moduleId].exports;
                                /******/ 		}
                            /******/ 		// Create a new module (and put it into the cache)
                            /******/ 		var module = installedModules[moduleId] = {
                                /******/ 			i: moduleId,
                                /******/ 			l: false,
                                /******/ 			exports: {}
                                /******/ 		};
                            /******/
                            /******/ 		// Execute the module function
                            /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
                            /******/
                            /******/ 		// Flag the module as loaded
                            /******/ 		module.l = true;
                            /******/
                            /******/ 		// Return the exports of the module
                            /******/ 		return module.exports;
                            /******/ 	}
                        /******/
                        /******/
                        /******/ 	// expose the modules object (__webpack_modules__)
                        /******/ 	__webpack_require__.m = modules;
                        /******/
                        /******/ 	// expose the module cache
                        /******/ 	__webpack_require__.c = installedModules;
                        /******/
                        /******/ 	// define getter function for harmony exports
                        /******/ 	__webpack_require__.d = function(exports, name, getter) {
                            /******/ 		if(!__webpack_require__.o(exports, name)) {
                                /******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
                                /******/ 		}
                            /******/ 	};
                        /******/
                        /******/ 	// define __esModule on exports
                        /******/ 	__webpack_require__.r = function(exports) {
                            /******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
                                /******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
                                /******/ 		}
                            /******/ 		Object.defineProperty(exports, '__esModule', { value: true });
                            /******/ 	};
                        /******/
                        /******/ 	// create a fake namespace object
                        /******/ 	// mode & 1: value is a module id, require it
                        /******/ 	// mode & 2: merge all properties of value into the ns
                        /******/ 	// mode & 4: return value when already ns object
                        /******/ 	// mode & 8|1: behave like require
                        /******/ 	__webpack_require__.t = function(value, mode) {
                            /******/ 		if(mode & 1) value = __webpack_require__(value);
                            /******/ 		if(mode & 8) return value;
                            /******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
                            /******/ 		var ns = Object.create(null);
                            /******/ 		__webpack_require__.r(ns);
                            /******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
                            /******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
                            /******/ 		return ns;
                            /******/ 	};
                        /******/
                        /******/ 	// getDefaultExport function for compatibility with non-harmony modules
                        /******/ 	__webpack_require__.n = function(module) {
                            /******/ 		var getter = module && module.__esModule ?
                                /******/ 			function getDefault() { return module['default']; } :
                                /******/ 			function getModuleExports() { return module; };
                            /******/ 		__webpack_require__.d(getter, 'a', getter);
                            /******/ 		return getter;
                            /******/ 	};
                        /******/
                        /******/ 	// Object.prototype.hasOwnProperty.call
                        /******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
                        /******/
                        /******/ 	// __webpack_public_path__
                        /******/ 	__webpack_require__.p = "/";
                        /******/
                        /******/
                        /******/ 	// Load entry module and return exports
                        /******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
                        /******/ })
                    /************************************************************************/
                    /******/ ({

                        /***/ "./src/js/index.js":
                        /*!*************************!*\
                          !*** ./src/js/index.js ***!
                          \*************************/
                        /*! no static exports found */
                        /***/ (function(module, exports) {

// const form = document.querySelector('.contacts .form');
// const hideText = document.querySelector('.contacts .hide-text');
// const fields = form.querySelectorAll('.contacts .required');
// const successForm = document.querySelector('.contacts .success-form');
//
// const userInfoWarnings = {
// 	textIsCorrect: {
// 		message: 'The field is filled incorrectly',
// 		pattern: /^[&#1072;-&#1103;&#1040;-&#1071;&#1025;&#1105;a-zA-Z -]{2,}$/,
// 	},
// 	emailIsCorrect: {
// 		message: 'The field is filled incorrectly',
// 		pattern: /^.+@.+\..+$/,
// 	},
// 	messageIsCorrect: {
// 		message: 'The field is filled incorrectly',
// 		pattern: /\S/,
// 	},
// 	allCorrect: {
// 		message: 'All fields are required',
// 	},
// };
//
// function fieldValidation(val, type) {
// 	const {
// 		emailIsCorrect,
// 		textIsCorrect,
// 		messageIsCorrect,
// 	} = userInfoWarnings;
// 	switch (type) {
// 		case 'email':
// 			return !emailIsCorrect.pattern.test(val) && emailIsCorrect.message;
// 		case 'name':
// 			return !textIsCorrect.pattern.test(val) && textIsCorrect.message;
// 		case 'subject':
// 			return !textIsCorrect.pattern.test(val) && textIsCorrect.message;
// 		case 'message':
// 			return !messageIsCorrect.pattern.test(val) && messageIsCorrect.message;
// 		default:
// 			return 'Unknown error';
// 	}
// }
// function obligatory(val, type) {
// 	const isEmpty = /^$/.test(val);
// 	if (isEmpty) {
// 		switch (type) {
// 			case 'email':
// 				return 'Email cannot be blank.';
// 			case 'name':
// 				return 'Name cannot be blank.';
// 			case 'subject':
// 				return 'Subject cannot be blank.';
// 			case 'message':
// 				return 'Message cannot be blank.';
// 			default:
// 				return 'This field is required';
// 		}
// 	}
// 	return fieldValidation(val, type);
// }
//
// const removeError = (item) => {
// 	const error = item.parentElement.querySelector('.error')
// 	if (error) {
// 		error.remove();
// 	}
// };
//
// const validate = (item) => {
// 	if (!!obligatory(item.value, item.id)) {
// 		const error = document.createElement('span');
// 		error.className = 'error';
// 		error.innerHTML = obligatory(item.value, item.id);
// 		item.parentElement.appendChild(error);
// 	}
// }
//
// fields.forEach((item) => {
// 	item.addEventListener('focus', function() {
// 		removeError(this);
// 	});
// 	item.addEventListener('blur', function() {
// 		validate(this)
// 	});
// });
//
//
// form.addEventListener('submit', function (event) {
// 	event.preventDefault();
//
// 	fields.forEach((item) => {
// 		removeError(item)
// 		validate(item)
// 	});
//
// 	const error = document.querySelector('.error');
// 	if (!error) {
// 		hideText.style.display = 'none';
// 		form.style.display = 'none';
// 		successForm.style.display = 'block';
// 		console.log('form send');
// 	}
// })
// scripts for main FAQ page
                            var categories = document.querySelectorAll('.faq .category__item .category__item-list');

                            var toggleButton = function toggleButton(item, count) {
                                var btn = document.createElement('button');
                                btn.type = 'button';
                                btn.className = 'btn category__item-btn';
                                btn.innerHTML = "View all ".concat(count);
                                item.parentElement.appendChild(btn);

                                btn.onclick = function () {
                                    if (item.classList.contains('active')) {
                                        item.classList.remove('active');
                                        item.style.maxHeight = 130 + 'px';
                                    } else {
                                        item.classList.add('active');
                                        item.style.maxHeight = item.scrollHeight + 'px';
                                    }

                                    btn.innerHTML = item.classList.contains('active') ? "Hide" : "View all ".concat(count);
                                };
                            };

                            categories.forEach(function (item) {
                                var countChildrenItem = item.childElementCount;

                                if (countChildrenItem > 4) {
                                    toggleButton(item, countChildrenItem);
                                    item.classList.add('toggle');
                                }
                            }); // scripts for item FAQ page

                            var articleItems = document.querySelectorAll('.articles-item .title');
                            articleItems.forEach(function (item) {
                                item.addEventListener('click', function () {
                                    var content = this.nextElementSibling;
                                    var parent = this.parentElement;

                                    if (parent.classList.contains('articles-item--active')) {
                                        parent.classList.remove('articles-item--active');
                                        content.style.maxHeight = 0;
                                    } else {
                                        parent.classList.add('articles-item--active');
                                        content.style.maxHeight = content.scrollHeight + 'px';
                                    }
                                });
                            });

                            /***/ })

                        /******/ });
                    //# sourceMappingURL=main.js.map
                </script>
            </div>
        </div>
    </article>
</main>