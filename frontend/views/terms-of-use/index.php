<?php

/* @var $this yii\web\View */

$this->title = 'Terms – TradeSanta';
?>
<main class="flex-grow-1">
    <article class="container-fluid py-3 py-md-5">
        <div class="container">
            <div class="wysiwyg">
                <div class="container">
                    <p>Disclaimer: We are not a registered broker-dealer or an investment advisor. You must trade and take sole responsibility to evaluate all information provided by this website and use it at your own risk. Investments in securities, commodities, currencies and other investment options are speculative and involve high degrees of risk. You could lose all or a substantial amount of your investment. You should carefully read all related information regarding any investment, and consult with your advisors, before investing.</p>
                    <ol>
                        <li>
                            <h3 class="mt-5 mb-3">Terms of Service</h3>
                            <p><em>Version 1.0; effective from October 2nd, 2018</em></p>
                            <p>TradeSanta (hereafter also “<strong>We</strong>”) offers online software as a service (SaaS) through the website&nbsp;<a href="https://tradesantas.com"><u>https://tradesantas.com</u></a> (hereafter – “<strong>Website</strong>”). Our software (hereafter – “<strong>Software</strong>”) enables you to trade and invest in cryptocurrencies by means of an automatic crypto trader bot – of which (solely) you control and configure the settings.</p>
                            <p>These terms and conditions (hereafter – “<strong>Terms</strong>”) apply to the relationship between TradeSanta and <strong>Users (</strong>hereafter – “<strong>you</strong>”<strong>)</strong> for any use of the Website and the Software that TradeSanta offers. It is prohibited to use the Website or Software without accepting these Terms.</p>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Definitions</h3>
                            <ol>
                                <li>
                                    <p><strong>Company: </strong>TradeSanta.</p>
                                </li>
                                <li>
                                    <p><strong>User(s): </strong>an individual private person(s) or legal entity making use of the Website or Software.</p>
                                </li>
                                <li>
                                    <p><strong>Software:</strong>&nbsp;the software developed by TradeSanta in order to enable Users to trade in crypto currencies on the crypto market, in the form of a crypto trader bot (hereafter – “<strong>bot</strong>”). The Software is available through the Website&nbsp;<a href="https://tradesantas.com"><u>https://tradesantas.com</u></a>.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Registration and personal account</h3>
                            <ol>
                                <li>
                                    <p>If you want to use our Website and Software, go to&nbsp;<a href="https://tradesantas.com"><u>https://tradesantas.com</u></a>. You will need to register and create a personal account in order to use the Website and Software.</p>
                                </li>
                                <li>
                                    <p>You must protect the login details of your account and keep your password strictly secret. We will assume that all actions taken from your account are done by you or on your behalf by the authorized person.</p>
                                </li>
                                <li>
                                    <p>You agree to provide up-to-date, complete and accurate information on your account. You agree to promptly update your personal account when necessary so that we can contact you as the case may be.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">General Information</h3>
                            <p>4.1 Software and Website can be used with a paid subscription</p>
                            <p>4.2 We reserve the right to change the prices going forward, however we will never change the prices retroactively.</p>
                            <p>4.3 If you decide not to prolong your subscription, all the will be deactivated automatically when the subscription expires and may be deleted after 24 hours of inactivity. If the bots are deactivated for more than 24 hours on a free trial, TradeSanta reserves the right to delete them. </p>
                            <p>4.4 Regardless of the type of subscription, the Software and Website are always subject to these Terms.</p>
                            <p>4.5 We do not bear any responsibility for fees and commissions connected to your participation in cryptotrading, and/or that may be charged by the third parties</p>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Available Plans:</h3>
                            <p>5.1.1 Basic Plan. By subscribing to Basic plan, for a period of 1 month you will receive the following additional features:</p>
                            <p>5.1.1.1 Ability to create and manage up to 49 trading bots.</p>
                            <p>5.1.1.2 Unlimited trading volume.</p>
                            <p>5.1.2 Advanced Plan. By subscribing to Advanced plan, for a period of 1 month you will receive the following additional features:</p>
                            <p>5.1.2.1 Ability to create and manage up to 99 trading bots.</p>
                            <p>5.1.2.2 Unlimited trading volume.</p>
                            <p>5.1.3 Maximum Plan. By subscribing to Maximum plan, for a period of 1 month you will receive the following additional features:</p>
                            <p>5.1.3.1 Ability to create and manage unlimited number of bots.</p>
                            <p>5.1.3.2 Unlimited trading volume.</p>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Withdrawal</h3>
                            <p>6.1 The User has the right to withdraw from the Website subject to the <a href="https://tradesantas.com/refund-policy"> Refund Policy. </a></p>
                            <p>6.2 The withdrawal will cancel all reciprocal obligations, except for the cases explicitly addressed in this Terms. &nbsp;</p>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Fair use of our Website and Software</h3>
                            <ol>
                                <li>
                                    <p>By using our Website and Software, you declare to be at least 18 years old.</p>
                                </li>
                                <li>
                                    <p>You may not use the Website and Software in such a way that you violate the law or any other applicable laws and regulations.</p>
                                </li>
                                <li>
                                    <p>As a condition for using the Website and Software, you agree not to provide any information, data or content to us or the Website and Software that is incorrect, inaccurate, incomplete or that violates any law or regulation. In addition,&nbsp;<strong>you represent that you will not, nor allow third parties over whom you have or may have immediate control to</strong>:</p>
                                    <ol>
                                        <li>
                                            <p>enter any non-public / secure areas of the Website or Software;&nbsp;</p>
                                        </li>
                                        <li>
                                            <p>send viruses, worms, junk mail, spam, chain letters, unsolicited offers or ads of any kind and for any purpose;&nbsp;</p>
                                        </li>
                                        <li>
                                            <p>investigate, scan or test the Website of Software or any other related system or network, or violate any security or authentication;</p>
                                        </li>
                                        <li>
                                            <p>use any automated systems of software to withdraw data from the Website (“screen-scraping”);&nbsp;</p>
                                        </li>
                                        <li>
                                            <p>make and distribute copies of the Website or Software;&nbsp;</p>
                                        </li>
                                        <li>
                                            <p>attempt to sell, distribute, copy, rent, sub-license, loan, merge, reproduce, alter, modify, reverse engineer, disassemble, decompile, transfer, exchange, translate, hack, distribute, harm or misuse the Website or Software;&nbsp;</p>
                                        </li>
                                        <li>
                                            <p>create derivative works of any kind whatsoever.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <p>You may not create an account under someone else’s name or act like someone else in any other way (i.e. commit “identity theft”).</p>
                                </li>
                                <li>
                                    <p>In case the account concerns a corporate account, only an authorized person is allowed to trade with the corporate account. It is the responsibility of the user of the corporate account that only authorized persons have access to the account.</p>
                                </li>
                                <li>
                                    <p>TradeSanta is entitled to (temporarily or permanently) block your account and deny you access to the Website, if we suspect abuse of the account or the Website. We can also block your account or deny you access to the platform if you do not comply with these Terms, including conditions and policies referenced herein.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Privacy</h3>
                            <ol>
                                <li>
                                    <p>We strictly follow industry best practices and adhere to the rules set forth in the General Data Protection Regulation, OPPA, CAN-SPAM and COPPA. When you make use of our Website and Software, we will collect certain personal data from you. In our Privacy Policy you can read which personal data we collect and for what purposes. The Privacy Policy is an integral part of this Terms. You can find our privacy policy here: <a href="https://tradesantas.com/privacy"><u>https://tradesantas.com/privacy</u></a>.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Intellectual property</h3>
                            <ol>
                                <li>
                                    <p>Please note that content, materials, information, and functionality available on the Website and the Software are protected in all WTO-member states. TradeSanta is the exclusive licensee of all intellectual property rights vesting in and relating to (all content made available through) the Website and the Software, such as – but not limited to – patents, patent applications, trademarks, trademark applications, database rights, service marks, trade names, copyrights, trade secrets, licenses, domain names, know-how, property rights and processes (“<strong>Intellectual Property Rights</strong>”).</p>
                                </li>
                                <li>
                                    <p>TradeSanta grants its Users a non-transferrable, non-exclusive, non-sublicensable and revocable license of the Website and Software as offered by us at&nbsp;<a href="https://tradesantas.com"><u>https://tradesantas.com</u></a> subject to limitations outlined in clause 5.</p>
                                </li>
                                <li>
                                    <p>Any other use of the Services or Content is expressly prohibited. All other rights in the Platform or Content are reserved by us and our licensors. You will not otherwise copy, transmit, distribute, sell, resell, license, de-compile, reverse engineer, disassemble, modify, publish, participate in the transfer or sale of, create derivative works from, perform, display, incorporate into another website, or in any other way exploit any of the Content or any other part of the Services in whole or in part for commercial or non-commercial purposes. If you violate any portion of the license, your permission to access and use the Platform will be terminated immediately. We reserve the right to seek legal remedies available in the applicable law in the case of such a violation.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Availability of the Website and Software and disclaimer of warranties</h3>
                            <ol>
                                <li>
                                    <p>TradeSanta will use reasonable efforts to make the Website and Software available at all times. However, User acknowledges that the Website and Software are provided over the internet and mobile networks and thus the quality and availability of the Website and Software may be affected by factors outside TradeSanta’s reasonable control.</p>
                                </li>
                                <li>
                                    <p>TradeSanta does not accept any responsibility whatsoever for unavailability of the Website and Software, or any difficulty or inability to download or access content, or any other communication system failure which may result in the Website or Software being unavailable (hereinafter - Outages). However, In case of Outages we will prolong your paid subscription for the number of full days the Services were unavailable due to the Outage.</p>
                                </li>
                                <li>
                                    <p>TradeSanta is not responsible for any support or maintenance regarding the Website or Software. TradeSanta may – at its own discretion – update, modify, or adapt the Website or Software and their functionalities from time to time to enhance the user experience. TradeSanta is not responsible for any downtime resulting from these actions.</p>
                                </li>
                                <li>
                                    <p>To the maximum extent permitted by applicable law, TradeSanta hereby disclaims all implied warranties regarding the availability of the Website and Software. The Website and Software are provided "as is" and "as available" without warranty of any kind.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Helpdesk, advice and disclaimer</h3>
                            <ol>
                                <li>
                                    <p>TradeSanta has a helpdesk where User can ask questions about the Website and Software. TradeSanta will only give advice about the functioning of the Website and Software.</p>
                                </li>
                                <li>
                                    <p>TradeSanta explicitly does not:</p>
                                    <ol>
                                        <li>
                                            <p>Give Users any personal advice on recommended settings for the Bots;</p>
                                        </li>
                                        <li>
                                            <p>Give Users any personal financial advice.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <p>TradeSanta may upload general tutorials and academy videos on the Website, about the functioning of the Website and Software.</p>
                                </li>
                                <li>
                                    <p>All tutorials, videos and templates uploaded by TradeSanta are general and contain in no way personal and/or financial advice. All use of these advice is at the sole risk of the User.</p>
                                </li>
                                <li>
                                    <p>Users may receive notifications about the transaction made in their TradeSanta account by using Telegram bot located at: t.me/tradesantabot. However, TradeSanta assumes no responsibility over the information received through above-mentioned service, since its functionality and availability lies beyond our reasonable control.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Risks and legal advice</h3>
                            <ol>
                                <li>
                                    <p>The User is aware of the accompanying risks of possessing, trading and using crypto currencies and takes full responsibility for these risks.</p>
                                </li>
                                <li>
                                    <p>No information provided by us shall be interpreted to constitute legal advice. You should bear in mind that the laws of your state of residence may prohibit transactions made with cryptocurrencies. In case you are uncertain of the status of cryptocurrencies in your jurisdiction, you should seek legal advice from a legal professional practising law in your jurisdiction.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Liability</h3>
                            <ol>
                                <li>
                                    <p>Nothing in these Terms shall exclude or limit TradeSanta’s liability when it cannot be excluded or limited under applicable law.</p>
                                </li>
                                <li>
                                    <p>TradeSanta is not liable to you for any (direct or indirect) damage you suffer as a result of the use of the Website or Software or the content provided thereon. For example, TradeSanta is not liable for:</p>
                                    <ol>
                                        <li>
                                            <p>the proper functioning of (hyper) links provided by the Website or Software;</p>
                                        </li>
                                        <li>
                                            <p>the (lack of) financial benefit for the Users through the use of the Website or Software;</p>
                                        </li>
                                        <li>
                                            <p>any situation where Users login details and/or password is stolen and any third party subsequently makes use of the Website or Software without User’s consent;</p>
                                        </li>
                                        <li>
                                            <p>any damage or alteration to User’s equipment including but not limited to computer equipment or a handheld device as a result of the use of the Website or Software;</p>
                                        </li>
                                        <li>
                                            <p>a failure to meet any of TradeSanta’s obligations under these Terms where such failure is due to events beyond TradeSanta’s reasonable control.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <p>The information and data on the “<strong>Group chat</strong>”&nbsp;on Telegram&nbsp;<!--noindex--><a href="https://t.me/joinchat/UXVKxRWC-f_hSDOS" rel="nofollow noopener" target="_blank"><span style="background-color: transparent; color: #000000; font-family: calibri; font-size: 11pt;">@Tradesanta</span></a><!--/noindex--> are User Generated. TradeSanta has no influence on the data and information that is transmitted between Users on the Group chat, and TradeSanta will not monitor any content on the User-Forum uploaded or shared by Users.&nbsp; Therefore, TradeSanta is not liable for any data and information on the Group chat. TradeSanta may remove content and information from the Group chat if we are notified that the content or information is unlawful, violating these Terms or is otherwise inappropriate.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Indemnification</h3>
                            <ol>
                                <li>
                                    <p>Users will indemnify, defend, and hold TradeSanta harmless from and against all liabilities, damages and costs (including settlement costs and reasonable attorneys’ fees) arising out of third-party claims regarding:</p>
                                    <ol>
                                        <li>
                                            <p>any injury or damages resulting from the behaviour of User related to the use of our Website and Software; and</p>
                                        </li>
                                        <li>
                                            <p>breach by User of these Terms or violation of any applicable law, regulation or order.</p>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Miscellaneous&nbsp;</h3>
                            <ol>
                                <li>
                                    <p>TradeSanta reserves the right to change these Terms. When we change these Terms in a significant way, we will notify Users by newsletter (if User has provided us with his e-mail address to this end) and post a notification on our Website along with the updated Terms. By continuing to use the Website, you acknowledge the most recent version of these Terms.</p>
                                </li>
                                <li>
                                    <p>If we do not enforce (parts of) these Terms, this cannot be construed as consent or waiver of the right to enforce them at a later moment in time or against another User.</p>
                                </li>
                                <li>
                                    <p>The User cannot transfer the rights and obligations from these Terms to third parties.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Links</h3>
                            <ol>
                                <li>
                                    <p>Users agree that TradeSanta takes no responsibility for other Sites or other resources offered by a third party over our Site. TradeSanta takes no responsibility for the contents, ads, projects or other resources on the Sites mentioned above. TradeSanta takes no responsibility for the commodities, services or information on the above-mentioned Sites</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Severability&nbsp;</h3>
                            <ol>
                                <li>
                                    <p>The invalidity or unenforceability of any provision of these Terms shall not affect the validity or enforceability of any other provision of these Terms. Any such invalid or unenforceable provision shall be replaced or be deemed to be replaced by a provision that is considered to be valid and enforceable and which interpretation shall be as close as possible to the intent of the invalid provision.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Entire agreement</h3>
                            <ol>
                                <li>
                                    <p>These Terms constitute the entire agreement between you and us. These Terms supersede all prior communications, contracts, or agreements between the parties concerning the subject matter addressed in these Terms, whether oral or written.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Force majeure</h3>
                            <ol>
                                <li>
                                    <p>In addition to other disclaimers, our performance under these Terms shall be excused in the event of interruption and/or delay due to, or resulting from, causes beyond our reasonable control, including but not limited to acts of God, acts of any government, war or other hostility, civil disorder, the elements, fire, flood, snowstorm, earthquake, explosion, embargo, acts of terrorism, power failure, equipment failure, industrial or labor disputes or controversies, acts of any third party data provider(s) or other third-party information provider(s), third-party software, or communication method interruptions.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h3 class="mt-5 mb-3">Complaints, comments and suggestions</h3>
                            <ol>
                                <li>
                                    <p>TradeSanta strives to give you the optimal service. If you have a complaint, comment or suggestion, you can contact us at&nbsp;<a href="mailto:team@tradesanta.com">team@tradesanta.com</a>&nbsp; Please provide us with your contact details, and a clear description and reason for your complaint. Complaints are usually processed within 30 working days.</p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </div>          </div>
        </div>
    </article>
</main>