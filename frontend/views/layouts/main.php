
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" data-lang-url-prefix="<?= Yii::$app->language ?>"  class>
    <head>
        <script>
            dataLayer = [{

            }];
        </script>


        <style>.async-hide {
                opacity: 0 !important
            } </style>
        <script>(function (window, documentElement, hideClassName, timeout) {
                documentElement.className += ' ' + hideClassName;
                let i = function () {
                    documentElement.className = documentElement.className.replace(RegExp(' ?' + hideClassName), '')
                };
                setTimeout(function () {
                    i();
                }, timeout);
            })(window, document.documentElement, 'async-hide', 150);</script>


        </noscript>

        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, shrink-to-fit=yes, viewport-fit=cover">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <meta name="description" content="TradeSanta is the best platform for trade automation. Use trading robots buy & sell signals, risk management tools to trade faster and smarter ✓ Robot works automatically for you 24/7">




        <meta property="og:type" content="website">
        <meta property="og:url" content="https://tradesanta.com/en">
        <meta property="og:title" content="TradeSanta – Automated Crypto Trading Bots">
        <meta property="og:description" content="TradeSanta is the best platform for trade automation. Use trading robots buy & sell signals, risk management tools to trade faster and smarter ✓ Robot works automatically for you 24/7">
        <meta property="og:site_name" content="Trade Santa">
        <meta property="og:image" content="images/index.png">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@TradeSanta">
        <meta name="twitter:creator" content="@TradeSanta">
        <meta name="twitter:title" content="TradeSanta – Automated Crypto Trading Bots">
        <meta name="twitter:description" content="TradeSanta is the best platform for trade automation. Use trading robots buy & sell signals, risk management tools to trade faster and smarter ✓ Robot works automatically for you 24/7">
        <meta name="twitter:image" content="https://tradesanta.com/img/index.png">
        <link rel="alternate" hreflang="en" href="https://tradesanta.com/en">
        <link rel="alternate" hreflang="ko" href="https://tradesanta.com/kr">
        <link rel="alternate" hreflang="pt-BR" href="https://tradesanta.com/br">
        <link rel="alternate" hreflang="tr" href="https://tradesanta.com/tr">
        <link rel="alternate" hreflang="ja" href="https://tradesanta.com/jp">
        <link rel="alternate" hreflang="zh" href="https://tradesanta.com/zh">
        <link rel="alternate" hreflang="es" href="https://tradesanta.com/es">
        <link rel="alternate" hreflang="ru" href="https://tradesanta.com/ru">
        <link rel="alternate" hreflang="fr" href="https://tradesanta.com/fr">
        <link href="https://tradesanta.com/en" rel="canonical">

        <link rel="icon" type="image/png" href="images/favicon.png">


        <!-- Defer loading css files @see https://developers.google.com/speed/docs/insights/OptimizeCSSDelivery#example -->

        <script>
            var loadDeferredStyles = function () {
                var addStylesNode = document.getElementById("deferred-styles");
                var replacement = document.createElement("div");
                replacement.innerHTML = addStylesNode.textContent;
                document.body.appendChild(replacement);
                addStylesNode.parentElement.removeChild(addStylesNode);
            };
            var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
            if (raf) raf(function () {
                window.setTimeout(loadDeferredStyles, 0);
            });
            else window.addEventListener('load', loadDeferredStyles);
        </script>


        <script>
            window.addEventListener("load", () => {
                setTimeout(() => {
                    "use strict";
                    !function () {
                        var t = window.driftt = window.drift = window.driftt || [];
                        if (!t.init) {
                            if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
                            t.invoked = !0, t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                                t.factory = function (e) {
                                    return function () {
                                        var n = Array.prototype.slice.call(arguments);
                                        return n.unshift(e), t.push(n), t;
                                    };
                                }, t.methods.forEach(function (e) {
                                t[e] = t.factory(e);
                            }), t.load = function (t) {
                                var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                                var i = document.getElementsByTagName("script")[0];
                                i.parentNode.insertBefore(o, i);
                            };
                        }
                    }();
                    drift.SNIPPET_VERSION = '0.3.1';
                    drift.load('frysytn4wna2');
                }, 5000);
            });
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-223744353-1">
        </script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-223744353-1');
        </script>
    </head>

<body class="d-flex flex-column">
<?php $this->beginBody() ?>



<!-- header -->
<header>

    <!-- banner-cookies -->


    <div class="banner-cookies container-fluid banner-cookies_hidden">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-8 col-sm-9 small">
                    By using our services you agree <a href="/en/cookies-policy" title="Cookies policy" class="text-white font-weight-bold">to our cookies  policy</a>                    </div>
                <div class="col-4 col-sm-3 text-right">
                    <a href="/en/site/allowcookies" id="allow_cookies_button" class="btn btn-light px-4">OK</a>
                </div>
            </div>
        </div>
    </div>

    <!-- /banner-cookies -->




    <div class="container-fluid bg-warning py-2">
        <div class="row px-3">
            <div class="col mx-auto" style="max-width: 1220px">
                <nav class="navbar navbar-expand-lg navbar-light ml-auto px-0" style="max-width: 1110px">
                    <a class="navbar-brand p-0" href="/">
                        <img src="fonts/logo.svg" alt="Trade santa – cryptocurrency trading bots" width="157" height="44">
                    </a>
                    <a class="btnAccount btn btn-primary btn-sm d-lg-none ml-auto mr-3 mr-sm-4" href="/en/account">Account</a>                    <button class="navbar-toggler border-0 p-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse overflow-auto invisible" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto align-items-center h-100 pb-lg-0" style="padding-bottom: 100px">
                            <li class="nav-item d-flex align-items-start d-lg-none w-100 p-0">

                                <div class="collapse-lang">

                                    <button class="btn p-0 pt-1 d-flex align-items-center text-white border-0" data-toggle="collapse" data-target="#langListMobile" aria-expanded="false" aria-controls="langListMobile">
                                        <img src="images/en.png" width="20" height="20" alt="en">
                                        <strong class="ml-2 text-uppercase">en</strong>
                                    </button>

                                    <div class="collapse" id="langListMobile">
                                        <a href="/site/change-language?language_id=kr" title="한국어" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/kr.png" width="20" height="20" alt="한국어">
                                            <span class="ml-2 text-uppercase">한국어</span>
                                        </a>
                                        <a href="/site/change-language?language_id=br" title="pt-br" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/br.png" width="20" height="20" alt="pt-br">
                                            <span class="ml-2 text-uppercase">PT-BR</span>
                                        </a>
                                        <a href="/site/change-language?language_id=tr" title="tr" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/tr.png" width="20" height="20" alt="tr">
                                            <span class="ml-2 text-uppercase">TR</span>
                                        </a>
                                        <a href="/site/change-language?language_id=jp" title="ja" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/jp.png" width="20" height="20" alt="ja">
                                            <span class="ml-2 text-uppercase">JA</span>
                                        </a>
                                        <a href="/site/change-language?language_id=zh" title="zh" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/zh.png" width="20" height="20" alt="zh">
                                            <span class="ml-2 text-uppercase">ZH</span>
                                        </a>
                                        <a href="/site/change-language?language_id=es" title="es" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/es.png" width="20" height="20" alt="es">
                                            <span class="ml-2 text-uppercase">ES</span>
                                        </a>
                                        <a href="/site/change-language?language_id=ru" title="ru" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/ru.png" width="20" height="20" alt="ru">
                                            <span class="ml-2 text-uppercase">RU</span>
                                        </a>
                                        <a href="/site/change-language?language_id=fr" title="fr" class="dropdown-item px-0 w-100 d-flex align-items-center">
                                            <img src="images/fr.png" width="20" height="20" alt="fr">
                                            <span class="ml-2 text-uppercase">FR</span>
                                        </a>
                                    </div>

                                </div>


                                <button class="btn btn-link ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-times fa-lg align-middle"></i>
                                </button>
                            </li>
                            <li class="nav-item pt-3 pt-lg-0 px-0 w-100 mb-3 d-lg-none">
                                <a class="btnGuest btn btn-outline-light mb-3 px-0 d-block" href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" rel="nofollow">Start</a>                                                     </li>
                            <li class="nav-item px-0 px-lg-3 w-100 active">
                                <a class="nav-link" href="/crypto-markets">
                                    Trading pairs                                </a>
                            </li>

                            <li class="nav-item px-0 px-lg-3 w-100">

                                <a class="nav-link" href="/faq">
                                    FAQ                                </a>
                            </li>
                            <li class="nav-item px-0 px-lg-3 w-100">
                                <a class="nav-link" href="/pricing">Pricing</a>
                            </li>
                            <li class="nav-item px-0 px-lg-3 w-100 active">
                                <a class="nav-link" href="/contact-us">
                                    Contact us                                </a>
                            </li>
                            <li class="nav-item pl-lg-0 mt-3 mt-lg-0">
                                <!--noindex--><a href="https://t.me/joinchat/UXVKxRWC-f_hSDOS" target="_blank" rel="nofollow noopener" class="btn btn-link px-0" style="width: 40px; text-decoration: none; font-size: 1rem;"><img src="fonts/icon-tg-circle.svg" alt="Telegram icon" style="width: 34px; height: 34px"></a><!--/noindex-->
                            </li>
                            <li class="nav-item nav-item-access pt-3 pt-lg-0 px-0 w-100 d-none d-lg-block">
                                <a class="btnGuest btn btn-link mx-3 px-0 d-inline-block" href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" rel="nofollow">Start</a>                                                                                          </li>
                        </ul>
                    </div>
                </nav>
            </div>

        </div>
    </div>

</header>
<!-- /header -->


<?=$content;?>

    <div>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&amp;display=swap" rel="stylesheet">
</div>
<!-- /content -->

<!-- /content -->


<!-- footer -->
<footer class="container-fluid bg-black text-white font-weight-light" id="footer" style="font-size: 1.25rem;">
    <div class="container">
        <div class="row justify-content-between align-items-center pt-5 pb-3">
            <div class="col-xl py-3 h3 m-0">
                <a class="navbar-brand p-0" href="/"><img src="fonts/logo-footer.svg" alt="Trade santa – cryptocurrency trading bots" width="247" height="64"></a>
            </div>
            <div class="col-xl-auto py-3 text-xl-right h3 m-0">
                Come join us:
                <!--noindex-->
                <!--noindex--><a href="https://t.me/joinchat/UXVKxRWC-f_hSDOS" target="_blank" rel="  nofollow noopener" class="ml-3" style="font-weight: 400"><img width="28" height="24" src="fonts/icon-tg.svg" alt="tradesanta telegram"></a><!--/noindex-->
                <!--/noindex-->
                <!--noindex-->
                <!--noindex--><a href="https://www.facebook.com/tradesanta" target="_blank" rel="  nofollow noopener" class="ml-3" style="font-weight: 400"><img width="17" height="30" src="fonts/icon-fb.svg" alt="tradesanta facebook"></a><!--/noindex-->
                <!--/noindex-->
                <!--noindex-->
                <!--noindex--><a href="https://twitter.com/trade_santa" target="_blank" rel="  nofollow noopener" class="ml-3" style="font-weight: 400"><img width="30" height="25" src="fonts/icon-tt.svg" alt="tradesanta twitter"></a><!--/noindex-->
                <!--/noindex-->
                <!--noindex-->
                <!--noindex--><a href="https://www.youtube.com/channel/UCXcw4IIdFNIbBJ2ptUunoQQ/" target="_blank" rel="  nofollow noopener" class="ml-3" style="font-weight: 400"><img width="31" height="23" src="fonts/icon-yt.svg" alt="tradesanta youtube"></a><!--/noindex-->
                <!--/noindex-->

            </div>
        </div>
        <div class="row justify-content-between pt-3 pb-5">
            <div class="col-sm-6 col-md-3 py-3">
                <div class="py-2 pb-3 text-white-50">Features
                </div>
                <ul class="list-unstyled m-0 font-weight-regular">

                    <li class="py-2"><a href="/faq" class="text-white">FAQ</a></li>
                    <li class="py-2"><a href="/referral-program" class="text-white">Referral program</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 py-3">

            </div>
            <div class="col-sm-6 col-md-3 py-3">
                <div class="py-2 pb-3 text-white-50">Contacts
                </div>
                <ul class="list-unstyled m-0 font-weight-regular">
                    <li class="py-2">
                        <!--noindex-->
                        <!--noindex--><a href="https://t.me/joinchat/UXVKxRWC-f_hSDOS" target="_blank" rel="  nofollow noopener" class="text-white">Telegram</a><!--/noindex-->
                        <!--/noindex-->
                    </li>
                    <li class="py-2">
                        <!--noindex-->
                        <!--noindex--><a href="https://www.youtube.com/channel/UCXcw4IIdFNIbBJ2ptUunoQQ/" target="_blank" rel="  nofollow noopener" class="text-white">YouTube</a><!--/noindex-->
                        <!--/noindex-->
                    </li>
                    <li class="py-2">
                        <!--noindex-->
                        <a href="/cdn-cgi/l/email-protection#d0a4b5b1bd90a4a2b1b4b5a3b1bea4b1feb3bfbd" target="_blank" rel="nofollow noopener" class="text-white"><span class="__cf_email__" data-cfemail="8efaebefe3cefafcefeaebfdefe0faefa0ede1e3">[email protected]</span></a>
                        <!--/noindex-->
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 py-3">
                <ul class="list-unstyled m-0 text-md-right">
                    <li class="py-2">
                        <!--noindex-->
                        <!--noindex--><a class="d-inline-block" href="https://apps.apple.com/au/app/tradesanta-crypto-trading-bot/id1457051269" rel="  nofollow noopener" target="_blank"><img class="img-fluid" width="128" height="40" src="images/apple.png" alt="crypto bot app on App Store"></a><!--/noindex-->
                        <!--/noindex-->
                    </li>
                    <li class="py-2">
                        <!--noindex-->
                        <!--noindex--><a class="d-inline-block" href="https://play.google.com/store/apps/details?id=com.tradesanta&hl=en" target="_blank" rel="  nofollow noopener"><img class="img-fluid" width="128" height="40" src="images/google.png" alt="trading bot app on Google Play"></a><!--/noindex-->
                        <!--/noindex-->
                    </li>
                </ul>
                <div class="mt-4 float-left float-md-right position-relative" style="width: 130px; height: 130px;">
                    <iframe src="https://cryptogeek.info/tools-widget/tradesanta?view=square&amp;theme=dark&amp;transparent=true" width="260" height="260" frameborder="0" style="
              transform: scale(0.5);
              transform-origin: 0 0;
              position: absolute;
          "></iframe>
                </div>
            </div>
            <div class="w-100">
            </div>
            <div class="col-md-3 col-sm-12 py-3 small text-white-50">
                <p>TradeSanta Global Limited<br>
                    BVI Business Company<br>
                    BVI COMPANY NUMBER:2069399
                </p>
            </div>
            <div class="col-md-9 col-sm-12 py-3 small text-white-50">
                <p>At this website, you may access software that enables you to trade and invest in cryptocurrencies by means of an automatic crypto trader bot – of which you solely control. Please consult with our Policies and Disclaimer before starting any trading.
                </p>
            </div>
            <div class="col-sm-3 col-md col-lg-2 py-2 small"><a href="/terms-of-use" class="text-white-50">Terms of use</a>
            </div>
            <div class="col-sm-3 col-md col-lg-2 py-2 small"><a href="/privacy" class="text-white-50">Privacy policy</a>
            </div>

            <div class="col-md-auto col-lg py-2 small text-md-right">© 2018 - 2022 TradeSanta
            </div>
        </div>
    </div>
</footer><!-- /footer -->


<script data-cfasync="false" src="js/email-decode.min.js"></script><script src="js/all-shared-2e7e34ce9b16a53ab8458306555c468c.js"></script>
<script src="js/all-public-27b4e989badf7ba10c2431e9c29b8f30.js"></script>

<script src="js/ScrollMagic.min.js" crossorigin="anonymous"></script>

<script>

    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    document.addEventListener("DOMContentLoaded", () => {


        if (document.querySelectorAll("[data-set-cookie]").length > 0) {

            let setCookieArr = document.querySelectorAll("[data-set-cookie]");

            setCookieArr.forEach(el => el.addEventListener("click", function setCookieByAttr(e) {
                document.cookie = this.dataset.setCookie;
                let event = new Event("cookie");
                document.dispatchEvent(event);
            }));

        }


        if (document.querySelectorAll("[data-b]").length > 0) {

            document.addEventListener("cookie", function closeBannerByCookie() {
                let hideBannerId = getCookie("closed-b");
                if (document.querySelectorAll("[data-b=\"" + hideBannerId + "\"]").length > 0) {
                    document.querySelector("[data-b=\"" + hideBannerId + "\"]").style.display = "none";
                }
            });

            let event = new Event("cookie");
            document.dispatchEvent(event);

        }


    });

    $(function () {

        function readCookie(name) {
            var nameEQ = encodeURIComponent(name) + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0)
                    return decodeURIComponent(c.substring(nameEQ.length, c.length));
            }
            return null;
        };

        function setCookie(name, value, options = {}) {

            options = {
                path: '/',
                ...options
            };

            if (options.expires instanceof Date) {
                options.expires = options.expires.toUTCString();
            }

            let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

            for (let optionKey in options) {
                updatedCookie += "; " + optionKey;
                let optionValue = options[optionKey];
                if (optionValue !== true) {
                    updatedCookie += "=" + optionValue;
                }
            }

            document.cookie = updatedCookie;
        }

        function isLoggedIn() {
            return readCookie('user_id');
        }

        function isCookiesAccepted(){
            let ok = readCookie('_allow_cookies')
            return Boolean(ok);
        }

        function showAccountButton(){
            $(".btnAccount").show();
            $(".btnGuest").remove();
            $(".navbar-collapse").removeClass('invisible');
        }

        function showSignInButtons(){
            $(".btnAccount").remove()
            $(".btnGuest").show();
            $(".navbar-collapse").removeClass('invisible');
        }

        function hideCookieBanner() {
            $(".banner-cookies").remove()
        }

        function showCookieBanner() {
            $(".banner-cookies").removeClass('banner-cookies_hidden')
        }

        if (isLoggedIn()) {
            showAccountButton();
            let userId = readCookie('user_id')
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'userId':userId,
            })
        } else {
            showSignInButtons();
        }

        if (!isCookiesAccepted()){
            showCookieBanner()
        }



        $("#allow_cookies_button").click(function (e) {
            e.preventDefault();
            $(this).parents('.banner-cookies').addClass('banner-cookies_hidden');
            setCookie("_allow_cookies", "1", {'max-age': 31536000, 'path':'/'})
        });

        //delay banner display
        setTimeout(function(w, d){
            var banner = document.querySelector('.sale-banner');
            if(!banner) return;
            banner.removeAttribute('hidden');
        }, 1000 * 10,window, document );

        if ($('.banner-image').length > 0) {
            if (document.cookie.search('tariff_plan_5_11_2020_default_banner_bs4=true') > 0) {
                $("#banner-image__close").parents('.banner-image').addClass('banner-image_hidden');
            } else {
                $("#banner-image__close").click(function (e) {
                    e.preventDefault();
                    $(this).parents('.banner-image').addClass('banner-image_hidden');
                    document.cookie = "tariff_plan_5_11_2020_default_banner_bs4=true; path=/";
                });
            }
        }


        // init controller
        var controller = new ScrollMagic.Controller();

        $('[data-counter]').html('0'); // reset counters to zero

        // build scene
        var counter = new ScrollMagic.Scene({
            triggerElement: "#metric",
            duration: 200
        })
            .addTo(controller)
            .on("enter", function (e) {

                /* counter */
                document.querySelectorAll('[data-counter]').forEach(function (el) {
                    let start;
                    const final = parseInt(el.dataset.counter, 10);
                    const duration = 1500;
                    const step = ts => {
                        if (!start) {
                            start = ts;
                        }
                        let progress = (ts - start) / duration;

                        if (progress * final < final) {
                            el.textContent = Math.floor(progress * final);
                        } else {
                            el.textContent = final;
                        }

                        if (progress < 1) {
                            requestAnimationFrame(step);
                        }
                    };
                    requestAnimationFrame(step)
                });
                /* /counter */

                counter.reverse(false);

            });


        if ($(".anim-show").length > 0) {

            document.querySelectorAll('.anim-show').forEach(elem => {
                elem.classList.add('anim-show_init');
            });

            $('.anim-show').each(function () {

                var animShow = new ScrollMagic.Scene({
                    triggerElement: this
                })
                    .setClassToggle(this, "anim-show_active")
                    .reverse(false)
                    .addTo(controller);

            });

        }

        if ($(".animation").length > 0) {
            document.querySelectorAll('.animation-main-block').forEach(elem => {
                elem.classList.add('animation-main-block_init');
            });
        }


        if ($(".animation").length > 0) {

            document.querySelectorAll('.animation').forEach(elem => {
                elem.classList.add('animation_init');
            });

            $('.animation').each(function () {

                var animCoin = new ScrollMagic.Scene({
                    triggerElement: this
                })
                    .setClassToggle(this, "animation_active")
                    .reverse(false)
                    .addTo(controller);

            });

        }


        /* video */
        if ($("video").length > 0) {

            var touchmoved;
            $('video').on('touchend', function (e) {
                if (touchmoved != true) {
                    this.paused ? this.play() : this.pause();
                }
            }).on('touchmove', function (e) {
                touchmoved = true;
            }).on('touchstart', function () {
                touchmoved = false;
            });

            $('video').hover(
                function () {
                    $(this).prop('controls', true);
                },
                function () {
                    $(this).prop('controls', false);
                }
            );

        }
        /* /video */

    })
</script>

<script type="text/javascript">(function(){window['__CF$cv$params']={r:'6f0179231a8396fc',m:'5DUdlm8Y3rKestCjrfR0kzl1Z7mvIVWqZQLZJ93jE64-1647978803-0-AbccDcEMX+P4lw9SCYJ04IlKS2TR9vE7EtqrTE3SHdxC/YYLzD4MKdESctkzTciWxj8GIonFV2j8+8HcNNPFcGWYgC9IIshxO5HcW4NZiDwKMslsGO7jgllrDXdDx/jLfQ==',s:[0x75c6c7e2a1,0x9548d8a0ee],}})();</script><script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" data-cf-beacon="{" rayId":"6f0179231a8396fc","token":"9bfa9588f815409994cf9cf46b1b9521","version":"2021.12.0","si":100}" crossorigin="anonymous"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(88003503, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/88003503" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<?php $this->endBody() ?>
        </body>
</html>
<?php $this->endPage() ?>

