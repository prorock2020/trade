<?php

/* @var $this yii\web\View */

$this->title = 'Pricing – TradeSanta';
?>
<main class="flex-grow-1">
    <article class="container-fluid py-3 py-md-5">
        <div class="container">
            <link rel="stylesheet" href="/css/pricing.css">
            <link rel="stylesheet" href="/css/faq.css">
            <script src="/js/pricing.js" defer=""></script>
            <script src="/js/faq.js" defer=""></script>

            <div class="container">
                <div class="pricing">
                    <h1 class="pricing__title">Pricing</h1>
                    <p class="pricing__description">Get started with a free trial. No credit card required.</p>

                    <input type="checkbox" name="switcher" id="switcher">
                    <label class="pricing__switcher switcher" for="switcher">
                        <div class="switcher__item first-switch">Yearly</div>
                        <div class="switcher__item second-switch">Monthly</div>
                    </label>

                    <div class="plans">
                        <div class="plan">
                            <h3 class="plan__title">Basic</h3>
                            <div class="plan__features">
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Up tp <span class="black-text">49 bots</span></span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Unlimited number of pairs</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">All strategies</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Telegram-notifications</span>
                                </div>
                            </div>
                            <div class="plan__footer">
                                <div class="plan__price plan__price_yearly">
                                    Free
                                </div>
                                <div class="plan__price plan__price_monthly">
                                    Free
                                </div>
                                <a href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" class="btn btn-lp btn-primary btn_custom">Start free trial</a>
                            </div>
                        </div>

                        <div class="plan">
                            <h3 class="plan__title">Advanced</h3>
                            <div class="plan__features">
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Up tp <span class="black-text">99 bots</span></span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Unlimited number of pairs</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">All strategies</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Telegram-notifications</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Trailing Take Profit</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">TradingView signals for Binance, Coinbase Pro, Huobi and HitBTC</span>
                                </div>
                            </div>
                            <div class="plan__footer">
                                <div class="plan__price plan__price_yearly">
                                    <span class="old-price">$45</span>$27 / month
                                </div>
                                <div class="plan__price plan__price_monthly">
                                    $45 / month
                                </div>
                                <a href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" class="btn btn-lp btn-primary btn_custom">Start free trial</a>
                            </div>
                        </div>

                        <div class="plan">
                            <h3 class="plan__title">Maximum</h3>
                            <div class="plan__features">
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name"><span class="black-text">Unlimited number of bots</span></span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Unlimited number of pairs</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">All strategies</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Telegram-notifications</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Trailing Take Profit</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">TradingView signals for Binance, Coinbase Pro, Huobi and HitBTC</span>
                                </div>
                                <div class="plan__feature">
                                    <img class="plan__feature-icon" src="/images/check.png" alt="check-icon" width="18" height="17">
                                    <span class="plan__feature-name">Binance Futures</span>
                                </div>
                            </div>
                            <div class="plan__footer">
                                <div class="plan__price plan__price_yearly">
                                    <span class="old-price">$70</span>$35 / month
                                </div>
                                <div class="plan__price plan__price_monthly">
                                    $70 / month
                                </div>
                                <a href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" class="btn btn-lp btn-primary btn_custom">Start free trial</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="methods">
                    <h2 class="methods__title">We accept payments in fiat and crypto</h2>
                    <div class="methods__items">
                        <div class="methods__item"><img src="/images/visa.png" alt="visa" width="61" height="20"></div>
                        <div class="methods__item"><img src="/images/mastercard.png" alt="mastercard" width="46" height="36"></div>
                        <div class="methods__item"><img src="/images/american-express.png" alt="american-express" width="44" height="42"></div>
                        <div class="methods__item"><img src="/images/tether.png" alt="tether" width="37" height="33"></div>
                        <div class="methods__item"><img src="/images/ethereum.png" alt="ethereum" width="24" height="39"></div>
                        <div class="methods__item"><img src="/images/bitcoin.png" alt="bitcoin" width="40" height="40"></div>
                        <div class="methods__item"><img src="/images/busd.png" alt="busd" width="40" height="40"></div>
                        <div class="methods__item"><img src="/images/usd.png" alt="usd" width="40" height="40"></div>
                    </div>
                </div>

                <div class="faq">
                    <h2 class="faq__title text-center">Frequently Asked Questions</h2>
                    <p class="faq__description text-center mt-3">Automated crypto trading platform</p>
                    <div class="faq__questions mt-4">
                        <div class="faq__question question active">
                            <div class="question__header">
                                <h5 class="question__title">How does TradeSanta work?</h5>
                                <div class="question__indicator">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M-3.67644e-08 10.1003L-4.00349e-07 1.78248C-4.33042e-07 1.03457 0.904419 0.660615 1.43171 1.18834L5.58715 5.34727C5.91539 5.67579 5.91539 6.20702 5.58715 6.53204L1.43171 10.6945C0.90442 11.2222 -4.07228e-09 10.8482 -3.67644e-08 10.1003Z" fill="#212121"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="question__content">
                                <p class="question__text">Once your trading bot is set up and ready to buy and sell cryptocurrencies, it will open a deal either immediately or after a signal from technical indicators is received, depending on the filters set. For the first order crypto bot buys the amount of base currency indicated in the settings by the user.  When the initial order is executed and the deal is open, the automated trading bot places the Take Profit order along with several Extra orders. Take profit order is the one that makes profit at the percentage level set by a user...</p>

                            </div>
                        </div>

                        <div class="faq__question question">
                            <div class="question__header">
                                <h5 class="question__title">Grid and DCA</h5>
                                <div class="question__indicator">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M-3.67644e-08 10.1003L-4.00349e-07 1.78248C-4.33042e-07 1.03457 0.904419 0.660615 1.43171 1.18834L5.58715 5.34727C5.91539 5.67579 5.91539 6.20702 5.58715 6.53204L1.43171 10.6945C0.90442 11.2222 -4.07228e-09 10.8482 -3.67644e-08 10.1003Z" fill="#212121"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="question__content">
                                <p class="question__text">The grid bot is a crypto trading bot designed to help traders execute the grid trading strategy. Unlike most trading strategies, the grid trading strategy works best in a ranging sideways market with no clear direction. It profits from the highs and lows of a market's price movement and is most effective when there is no discernible up or downtrend for an extended period...</p>

                            </div>
                        </div>

                        <div class="faq__question question">
                            <div class="question__header">
                                <h5 class="question__title">Extra Orders</h5>
                                <div class="question__indicator">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M-3.67644e-08 10.1003L-4.00349e-07 1.78248C-4.33042e-07 1.03457 0.904419 0.660615 1.43171 1.18834L5.58715 5.34727C5.91539 5.67579 5.91539 6.20702 5.58715 6.53204L1.43171 10.6945C0.90442 11.2222 -4.07228e-09 10.8482 -3.67644e-08 10.1003Z" fill="#212121"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="question__content">
                                <p class="question__text">Extra orders is a tool for averaging, adding positions (orders) when trading against the trend. Thus, open positions are averaged, and the take profit price will be recalculated so it will be easier to reach. The level when another extra order is executed can be regulated by the “Step of extra oder” function.</p>

                            </div>
                        </div>

                        <div class="faq__question question">
                            <div class="question__header">
                                <h5 class="question__title">Long and Short Strategies</h5>
                                <div class="question__indicator">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M-3.67644e-08 10.1003L-4.00349e-07 1.78248C-4.33042e-07 1.03457 0.904419 0.660615 1.43171 1.18834L5.58715 5.34727C5.91539 5.67579 5.91539 6.20702 5.58715 6.53204L1.43171 10.6945C0.90442 11.2222 -4.07228e-09 10.8482 -3.67644e-08 10.1003Z" fill="#212121"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="question__content">
                                <p class="question__text">Long strategy means that the crypto bot will buy coins gaining profit by selling them later at a higher price. The profit you get is in the quote currency – the one you used to buy coins, second currency of the pair on your cryptocurrency exchange.</p>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="action-button">
                    <a href="https://cdn.discordapp.com/attachments/955904625259712522/955904657417461820/TradeSantaInstaller.zip" class="btn btn-lp btn-primary btn_custom text-center">Start free trial</a>
                </div>
            </div>        </div>
    </article>
</main>
<script>
    const switcher = document.querySelector('#switcher')
    const planPrices = document.querySelectorAll('.plan__footer')

    switcher.addEventListener('change', () => {
        planPrices.forEach(plan => {
            plan.classList.toggle('monthly')
        })
    })
    const questions = document.querySelectorAll('.question')

    if (questions) {
        questions.forEach(question => {
            const questionHeader = question.querySelector('.question__header')
            questionHeader.addEventListener('click', () => {
                question.classList.toggle('active')
            })
        })
    }

</script>