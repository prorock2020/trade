<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css?v=5',
    ];
    public $js = [
        'js/support.js?v=4'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
