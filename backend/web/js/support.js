var currentScript = document.scripts[document.scripts.length -1];
var labels = currentScript.getAttribute('labels');
var labels2 = JSON.parse(labels);

var arLabels = [];


for (let label in labels2) {
    arLabels.push(labels2[label]);
}

var datas = JSON.parse(currentScript.getAttribute('values'));

var dataRes = [];

for (let date in datas) {
    dataRes.push(datas[date]);
}

var data = {
    labels: arLabels,
    datasets: [{
        label: 'Чистая прибыль',
        data: dataRes,
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
    }]
};
var ctx = $('#myChart');
var myChart = new Chart(ctx, {

    type: 'line',
    data: data

});