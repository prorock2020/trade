<?php

use yii\helpers\Html;

?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">

        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/" class="nav-link">Статистика</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/cards" class="nav-link">Карты</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/cards/create" class="nav-link">Добавить карту</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/orders" class="nav-link">Заказы</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/orders/create" class="nav-link">Добавить заказ</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/charges" class="nav-link">Расходы</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/admin/charges/create" class="nav-link">Добавить расход</a>
        </li>
        <?php
        if (Yii::$app->user->isGuest) {

        } else {
            echo  '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        ?>
    </ul>
</nav>
<!-- /.navbar -->