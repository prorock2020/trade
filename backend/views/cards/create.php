<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cards */

$this->title = 'Добавить карту';
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

