<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cards */

$this->title = 'Update Cards: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cards-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
