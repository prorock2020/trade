<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cards */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<?=$form->errorSummary($model);?>
<div class="cards-form">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Данные</h3>
        </div>
        <div class="card-body">


            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-6">

                            <?=
                            $form->field($model, 'number',[
                               // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                               "options"=>[
                                       "class"=>"input-group"
                               ]
                               // "class"=>'input-group'
                            ])->textInput(['maxlength' => true,'placeholder' => "Номер карты"])->label(false)->error(false)
                            ?>


                     </div>
                    <div class="col-lg-2">

                        <?=
                        $form->field($model, 'mm',[
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "mm"])->error(false)->label(false)
                        ?>


                    </div>
                    <div class="col-lg-2">
                        <?=
                        $form->field($model, 'yy',[
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "yy"])->error(false)->label(false)
                        ?>



                    </div>
                    <div class="col-lg-2">
                        <?=
                        $form->field($model, 'cvv',[
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "cvv"])->error(false)->label(false)
                        ?>



                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-6">

                        <?=
                        $form->field($model, 'kh_name',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Имя владельца"])->label(false)->error(false)
                        ?>


                    </div>
                    <div class="col-lg-2">

                        <?=
                        $form->field($model, 'state',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Штат"])->label(false)->error(false)
                        ?>


                    </div>
                    <div class="col-lg-4">

                        <?=
                        $form->field($model, 'city',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Город"])->label(false)->error(false)
                        ?>


                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-8">

                        <?=
                        $form->field($model, 'address',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Адрес"])->label(false)->error(false)
                        ?>


                    </div>
                    <div class="col-lg-4">

                        <?=
                        $form->field($model, 'zip',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "ZIP"])->label(false)->error(false)
                        ?>


                    </div>

                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-8">

                        <?=
                        $form->field($model, 'email',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Email"])->label(false)->error(false)
                        ?>


                    </div>
                    <div class="col-lg-4">

                        <?=
                        $form->field($model, 'email_pass',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Пароль"])->label(false)->error(false)
                        ?>


                    </div>

                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-8">

                        <?=
                        $form->field($model, 'recovery_email',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Email резерв"])->label(false)->error(false)
                        ?>


                    </div>
                    <div class="col-lg-4">

                        <?=
                        $form->field($model, 'phone',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Телефон"])->label(false)->error(false)
                        ?>


                    </div>

                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-12">

                        <?=
                        $form->field($model, 'comment',[
                            // "template" => "<div class=\"input-group-prepend\"><span class=\"input-group-text\">№</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textarea(['maxlength' => true,'placeholder' => "Комментарий"])->label(false)->error(false)
                        ?>


                    </div>


                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-lg-4">
                        <?php
                        echo '<label class="form-label">Дата покупки</label>';
                        echo \kartik\date\DatePicker::widget([
                            'model'=>$model,
                            'attribute' => 'created_at',
                            'type' => \kartik\date\DatePicker::TYPE_INPUT,

                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'todayBtn' => true,
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-lg-4">

                        <?=
                        $form->field($model, 'price',[
                            "template" => "<label style='width: 100%' class=\"form-label d-block\">{label}</label><div class=\"input-group-prepend\"><span class=\"input-group-text\">$</span></div>\n{input}\n{hint}\n{error}",
                            "options"=>[
                                "class"=>"input-group"
                            ]
                            // "class"=>'input-group'
                        ])->textInput(['maxlength' => true,'placeholder' => "Цена"])->error(false)
                        ?>


                    </div>
                </div>
            </div>
            <div class="input-group input-group-sm">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
            <!-- /input-group -->
        </div>
        <!-- /.card-body -->
    </div>






</div>
<?php ActiveForm::end(); ?>