<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Charges */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charges-form">
    <div class="cards-form">
        <div class="card card-info">

            <div class="card-body">
    <?php $form = ActiveForm::begin(); ?>
<?=$form->errorSummary($model);?>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>


                <div class="mb-3">
                    <?php
                    echo '<label class="form-label">Дата </label>';
                    echo \kartik\date\DatePicker::widget([
                        'model'=>$model,
                        'attribute' => 'created_at',
                        'type' => \kartik\date\DatePicker::TYPE_INPUT,

                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                        ]
                    ]);
                    ?>
                </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
