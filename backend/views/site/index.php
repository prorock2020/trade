<?php

/* @var $this yii\web\View */

$this->title = 'Статистика';
?>
<div class="row">
    <div class="col-lg-4">
        <?php

        $allOrders = \common\models\Orders::getAll();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>count($allOrders),
            'text'=>"Всего заказов",
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => 'На сумму '.\common\models\Orders::getSum($allOrders)
            ],
        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?php

        $progressOrders = \common\models\Orders::getProgressOrders();
        $progressSum = \common\models\Orders::getSum($progressOrders);
        $wastedSum = \common\models\Orders::getWasted($allOrders);
        $cardsSum = \common\models\Cards::getAllSum();
        $chargesSum = \common\models\Charges::getAllSum();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>($progressSum-$wastedSum-$cardsSum-$chargesSum),
            'text'=>'Чистая прибыль',
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => "Потрачено средств ".($wastedSum+$cardsSum+$chargesSum),
            ],
        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?php
        $orders = \common\models\Orders::getConfirmed();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>count($orders),
            'text'=>"Ожидают отправки",
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => 'На сумму '.\common\models\Orders::getSum($orders)
            ],
        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?php
        $orders = \common\models\Orders::getShipment();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>count($orders),
            'text'=>"Отправлены",
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => 'На сумму '.\common\models\Orders::getSum($orders)
            ],

        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?php
        $orders = \common\models\Orders::getCancel();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>count($orders),
            'text'=>"Не дошли",
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => 'На сумму '.\common\models\Orders::getSum($orders)
            ],
        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?php
        $orders = \common\models\Orders::getSuccess();
        echo \hail812\adminlte\widgets\InfoBox::widget([
            "number"=>count($orders),
            'text'=>"Доставлены",
            'icon' => 'fas fa-shopping-cart',
            'progress' => [
                'width' => '100%',
                'description' => 'На сумму '.\common\models\Orders::getSum($orders)
            ],
        ]);
        ?>
    </div>
</div>

<?
$formatter = \Yii::$app->formatter;
    $daysSum = [];
    $ordersLabels = [];
    $cardsArray = [];

    foreach ($allOrders as $order)
    {
        $kh = $order->kh;
        $cardsArray[$kh->id] = $kh;

        /**
         *  var $order
         */
        $date = $formatter->asDate($order->created_at, 'Y-MM-dd');
        $ordersLabels[] =  $date;
        if(empty($daysSum[$date])){
            $daysSum[$date] = 0;
        }
        if(!$order->isCancel())
        {
            $daysSum[$date] += $order->calcSum();
        }
        $daysSum[$date] -= $order->wasted;

    }

    foreach ($cardsArray as $card)
    {

        $cardDate = $formatter->asDate($card->created_at, 'Y-MM-dd');
        $ordersLabels[] = $cardDate;
        if(empty($daysSum[$cardDate])){
            $daysSum[$cardDate] = 0;
        }
        $daysSum[$cardDate] -= $card->price;
        print_r($card->price);

    }


    $arCharges = \common\models\Charges::findAll(["user_id"=>YII::$app->user->id]);
    foreach ($arCharges as $k=>$charge)
    {
        $chargeDate = $formatter->asDate($charge->created_at, 'Y-MM-dd');
        $ordersLabels[] = $chargeDate;
        if(empty($daysSum[$chargeDate])){
            $daysSum[$chargeDate] = 0;
        }
        $daysSum[$chargeDate] -= $charge->price;
    }



    $ordersLabels = array_unique($ordersLabels);

    asort($ordersLabels);
    $sortArLabels = [];
    $i=0;
    foreach ($ordersLabels as $label){
        $sortArLabels[$i] = $label;
        $i++;
    }
    ksort($daysSum);
    $ordersLabels = $sortArLabels;

print_r($ordersLabels);




?>
<div style="width: 100%; height: 500px">
    <canvas id="myChart" width="1000" height="400"></canvas>
</div>




<?php
$bundle = \hail812\adminlte3\assets\PluginAsset::register($this);
$bundle->js[] = "chart.js/Chart.js";
$this->registerJsFile("@web/js/support.js?v=4", [
    'labels'=>json_encode($ordersLabels,true),
    'values'=>json_encode($daysSum),
    'depends' => ['backend\assets\AppAsset',],

]);
