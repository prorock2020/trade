<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">
    <div class="cards-form">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Данные</h3>
            </div>
            <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-lg-6">




                        <?
                             $authors = \common\models\Cards::find()->orderBy(['id'=>'DESC'])->all();
                         // формируем массив, с ключем равным полю 'id' и значением равным полю 'name'
                            $items = \yii\helpers\ArrayHelper::map($authors,'id','kh_name');
                            krsort($items);
                            $params = [
                                'prompt' => 'Укажите Карту'
                            ];?>
                        <?= $form->field($model, 'card_id')->textInput()->widget(\kartik\select2\Select2::classname(), [
                            'data' => $items,
                            'options' => ['placeholder' => 'Выбери КХ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]);
                        ?>
                        </div>
                        <div class="col-lg-6">

                            <?=
                            $form->field($model, 'number',[
                                "options"=>[
                                    "class"=>"input-group"
                                ]
                                // "class"=>'input-group'
                            ])->textInput(['maxlength' => true,'placeholder' => "Номер заказа"])
                            ?>


                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'product')->textarea(['rows' => 1]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'tracking')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'percent')->textInput() ?>
                    </div>
                </div>




                <div class="mb-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'drop_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'drop_address')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>
                </div>






                    <div class="mb-3">
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'wasted')->textInput(['maxlength' => true]) ?>

                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                    <?php
                        echo '<label class="form-label">Дата заказа</label>';
                        echo \kartik\date\DatePicker::widget([
                            'model'=>$model,
                            'attribute' => 'created_at',
                            'type' => \kartik\date\DatePicker::TYPE_INPUT,

                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'todayBtn' => true,
                            ]
                        ]);
                    ?>
                    </div>
                    <div class="mb-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <?= $form->field($model, 'status')->dropDownList(\common\models\Orders::getStatusList());

//                                ->widget(\kartik\select2\Select2::classname(), [
//                                    'data' => \common\models\Orders::getStatusList(),
//                                    'hideSearch'=>true,
//                                    'value'=>$model->status,
//                                    'options' => ['placeholder' => 'Select a state ...'],
//                                    'pluginOptions' => [
//                                        'allowClear' => true
//                                    ]
//                                ]);
                                ?>
                            </div>

                        </div>
                    </div>






                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<style>
    label {
        display: block !important;
        width: 100% !important;
    }

</style>