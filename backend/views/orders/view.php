<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php

    $attributes = [
        ['columns' =>[
            [
                'group'=>true,
                'label'=>'Заказ',
                'displayOnly'=>true,
                'groupOptions'=>['class'=>'text-left']
            ],


        ]
        ],
        ['columns' =>
            [
                [

                    'attribute'=>'number',

                ],
                [
                    'attribute'=>'product',

                ],


            ],

        ],
        ['columns' =>
            [
                [
                    'attribute'=>'tracking',

                ],
                [
                    'attribute'=>'price',

                ],
                [
                    'attribute'=>'percent',

                ],


            ],

        ],
        ['columns' =>
            [
                [
                    'attribute'=>'drop_name',

                ],
                [
                    'attribute'=>'drop_address',

                ],
                [
                    'attribute'=>'comment',

                ],


            ],

        ],
        ['columns' =>
            [
                [
                    'attribute'=>'created_at',

                ],
                [
                    'attribute'=>'status',
                    'value' => function($data,$data2){

                        return \common\models\Orders::getStatus($data2->model->status);
                    },


                ],



            ],

        ]

    ];

        echo \kartik\detail\DetailView::widget([
            'model'=>$model,

            'hover'=>true,
            'mode'=>\kartik\detail\DetailView::MODE_VIEW,
            'panel'=>[
                'heading'=>'Заказ # ' . $model->number,
                'type'=>\kartik\detail\DetailView::TYPE_INFO,
            ],
            'attributes'=>$attributes

        ]);




    $kh = $model->kh;
    $attributes = [
        ['columns' =>[
            [
                'group'=>true,
                'label'=>'КХ',
                'displayOnly'=>true,
                'groupOptions'=>['class'=>'text-left']
            ],


        ]
        ],
        ['columns' =>
            [
                [

                    'attribute'=>'kh_name',

                ],



            ],

        ],
        ['columns' =>
            [
                [

                    'attribute'=>'number',

                ],
                [
                    'attribute'=>'mm',

                ],
                [
                    'attribute'=>'yy',

                ],
                [
                    'attribute'=>'cvv',

                ],


            ],

        ],
        ['columns' =>
            [
                [

                    'attribute'=>'state',

                ],
                [
                    'attribute'=>'city',

                ],
                [
                    'attribute'=>'address',

                ],
                [
                    'attribute'=>'zip',

                ],


            ],

        ],
        ['columns' =>
            [
                [

                    'attribute'=>'email',

                ],
                [
                    'attribute'=>'email_pass',

                ],
                [
                    'attribute'=>'recovery_email',

                ],
                [
                    'attribute'=>'phone',

                ],


            ],

        ],
        ['columns' =>
            [
                [

                    'attribute'=>'comment',

                ],



            ],

        ],

    ];

    echo \kartik\detail\DetailView::widget([
        'model'=>$kh,

        'hover'=>true,
        'mode'=>\kartik\detail\DetailView::MODE_VIEW,
        'panel'=>[
            'heading'=>'КХ # ' . $kh->kh_name,
            'type'=>\kartik\detail\DetailView::TYPE_INFO,
        ],
        'attributes'=>$attributes

    ]);
    ?>


</div>
