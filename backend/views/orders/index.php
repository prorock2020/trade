<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <div class="card">
        <div class="card-body">
    <p>
        <?= Html::a('Добавить заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


        <?= \hail812\adminlte\widgets\FlashAlert::widget([]);?>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-bordered'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'rowOptions'=>function($model){
//            return ['class' => "alert-".\common\models\Orders::getColor($model->status)];
//        },

        'columns' => [

            [
                'attribute'=>'number',


            ],

            'product:ntext',
            'tracking',

            // Другой вариант
            [
                'attribute'=>'card_id',

                'content'=>function($data){
                    return $data->kh->kh_name;
                }
            ],

            //'card_id',
            'drop_name',
            'drop_address:ntext',
            //'comment',
            //'created_at:ntext',
            //'updated_at:ntext',
            'price',
            [
                'header'=>'Сумма выплаты',

                'content'=>function($data){
                    return $data->price/100*$data->percent;
                }
            ],
            'wasted',
            [
                'attribute'=>'status',
                'contentOptions' => function($data){
                    return ['class' => "alert-".\common\models\Orders::getColor($data->status)];

                },
                'content'=>function($data){
                    return \common\models\Orders::getStatus($data->status);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<div style="display:block">'.Html::a('<i class="fas fa-eye"></i>', $url, [
                            'title' => Yii::t('app', 'lead-view'),
                        ]). '</div>';
                    },

                    'update' => function ($url, $model) {
                        return '<div style="display:block">'.Html::a('<i class="fas fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'lead-update'),
                        ]). '</div>';
                    },

                    'delete' => function ($url, $model,$key) {
                        return '<div style="display:block">'.Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                            'title' => Yii::t('app', 'lead-update'),

                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                        ]). '</div>';
                    },
                ],


            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

        </div>
    </div>
</div>
